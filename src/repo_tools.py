import itertools
import operator
import os
import sys
import subprocess
import pandas as pd
import numpy as np
import statsmodels.stats.multitest as multi
from urllib.request import urlretrieve
from scipy.stats import hypergeom


NCBI_ENS_URL = 'ftp://ftp.ncbi.nih.gov/gene/DATA/gene2ensembl.gz'
FUNCOUP_URL = 'https://funcoup5.scilifelab.se/downloads/download.action?type=network&instanceID=24480085&fileName=FC5.0_H.sapiens_compact.gz'


def download_file(filename, url, force_download=False):
    """
    Download a zipped file from url. unzip it and store it.
    :param force_download: bool (optional)
        If True download data
    :param filename: string
        path to save the data
    :param url: string
        web location of the data
    :return filename: string
        location of the downloaded file
    """
    zipped = f'{os.path.splitext(filename)[0]}.gz'
    if force_download or not os.path.exists(filename):
        urlretrieve(url, zipped)
        pd.read_csv(zipped, compression='gzip', sep='\t').to_csv(filename, index=False, sep='\t')
    return filename


def make_ens_ncbi_mapping(mapping_file='../data/raw/ncbi_ens.tsv', overwrite=False):
    """
    Make a mapping of ensembl and gene ids
    :param mapping_file: string (optional)
        location of the mapping file
    :param overwrite: bool (optional)
        if True download the mapping
    :return: dictionary
        mapping dictionary with ens as keys and gene ids as values
    """
    mapping = {}
    if not os.path.exists(mapping_file) or overwrite:
        download_file(filename=mapping_file, url=NCBI_ENS_URL)

    with open(mapping_file, 'r') as f:
        lines = f.read().split('\n')
        for line in lines:
            try:
                ncbi, ens = line.split('\t')
                try:
                    mapping[ens].add(ncbi)
                except:
                    mapping[ens] = {ncbi}
            except:
                pass
    print(f'Found {len(mapping.keys())} mappings')
    return mapping


def network_ens_to_ncbi(mapping, network='../data/external/funcoup.tsv',
                        ncbi_net_sif='../data/processed/funcoup.sif', overwrite=False):
    """
    Convert a weighted edge list to sif (simple interaction format) format and translate ensembl to gene id identifiers
    :param mapping: dictionary
        mapping dictionary with ens as keys and gene ids as values
    :param network: string (optional)
        file path to a weighted edge list
    :param ncbi_net_sif: string (optional)
        file path to the output sif file
    :param overwrite: bool (optional)
        if True download even if the file is present
    :return: ncbi_sif: pandas.DataFrame
        the network in sif format
    """
    if os.path.exists(ncbi_net_sif) and not overwrite:
        return pd.read_csv(ncbi_net_sif, header=None, sep='\t', names=['node1', 'pfc', 'node2'])

    if not os.path.exists(network):
        download_file(filename=network, url=FUNCOUP_URL)

    edges = []
    with open(network, 'r') as f:
        lines = f.read().split('\n')
        for line in lines[1:]:
            try:
                pfc, fbs, node1, node2 = line.split('\t')
                for ncbi1 in mapping[node1]:
                    for ncbi2 in mapping[node2]:
                        edges.append([ncbi1, pfc, ncbi2])
            except:
                pass
    ncbi_sif = pd.DataFrame(edges, columns=['node1', 'pfc', 'node2'], dtype='str')
    ncbi_sif.to_csv(ncbi_net_sif, index=False, header=False, sep='\t')
    print(f'\n{ncbi_sif.shape[0]} nodes in the ncbi sif network')
    return ncbi_sif


def get_network(net_df=None, network_sif_path='../data/processed/funcoup.sif', edge_score_cutoff=None,
                binox_format=False, overwrite=False):
    """
    Read in sif network and apply edge score cutoff
    :param overwrite: bool (optional)
        if True write to file even if file exists
    :param net_df: pandas.DataFrame (optional)
        dataframe of sif network
    :param network_sif_path: string (optional)
        location of sif file
    :param edge_score_cutoff: float (optional)
        cutoff for edge weights
    :param binox_format: bool (optional)
        if True, run binox_format function
    :return: net or net_full: pandas.DataFrame
        the tsv network with/without cutoff
    """
    out_file_root = os.path.splitext(network_sif_path)[0]

    if net_df is None:
        net_df = pd.read_csv(network_sif_path, sep=r'\s+', header=None, usecols=[0, 1, 2],
                             names=['node1', 'score', 'node2'], dtype={'node1': object, 'score': float,
                                                                       'node2': object})
    net_full = net_df[['node1', 'node2', 'score']]

    if edge_score_cutoff is not None:
        net = net_full[net_full.score >= edge_score_cutoff]
        out_file_cutoff_root = f'{out_file_root}.{edge_score_cutoff}'
        if not os.path.exists(f'{out_file_cutoff_root}.sif') or overwrite:
            net.to_csv(f'{out_file_cutoff_root}.sif', sep=' ', columns=['node1', 'score', 'node2'],
                       index=False, header=False)
            print(f'Wrote to {out_file_cutoff_root}.sif')
        if binox_format is True:
            network_binox_format(network_df=net, binox_net_file=f'{out_file_cutoff_root}.binox', overwrite=overwrite)
        return net
    if binox_format is True:
        network_binox_format(network_df=net_full, binox_net_file=f'{out_file_root}.binox')

    return net_full


def network_binox_format(network_df, binox_net_file='../data/processed/funcoup.binox', overwrite=False):
    """
    Convert network from sif to binox format
    :param network_df: pandas.DataFrame
        sif network dataframe
    :param binox_net_file: string (optional)
        output file location
    :param overwrite: bool (optional)
        if True overwrite existing binox file
    :return: pandas.DataFrame
        binox network dataframe
    """
    if os.path.exists(binox_net_file) and not overwrite:
        return pd.read_csv(binox_net_file, header=0, sep=r'\s+')

    binox = network_df.rename(columns={'node1': '#node1'})
    binox.to_csv(binox_net_file, sep='\t', index=False, columns=['#node1', 'node2', 'score'])
    print(f'Wrote to binox_net_file.')
    return binox


def clean_binox_output(binox_result_file='../data/processed/network.tsv', net_size=None, with_flags=True,
                       flag_file='../data/processed/proximity.dat', overwrite=False):
    """
    Parse BinoX output and add NEAT calculation
    :param with_flags:
    :param net_size:
    :param overwrite: bool (optional)
        if True overwrite the existing binox group file
    :param flag_file: string (optional)
        path to the proximity.dat file containing flags
    :param binox_result_file: string (optional)
        path to the binox file
    :return: pd.DataFrame
        dataframe with binox output including classes
    """
    binox_flag_file = f'{binox_result_file}.flag'
    if os.path.exists(binox_flag_file) and not overwrite:
        return pd.read_csv(binox_flag_file, header=0, sep='\t')

    # Clean up binox results and merge with flags from proximity file
    binox_results = pd.read_csv(binox_result_file, sep='\t', dtype={'#4.pvalue': np.float64, '#5.FDR': np.float64})
    binox_results.columns = binox_results.columns.str.replace(r'#\d+:', '')  # clean column names
    binox_results.NameGroupB = binox_results.NameGroupB.str.lower()
    new_names = {'NameGroupA': 'group', 'NameGroupB': 'disease', 'groupSize.first': 'n_drug',
                 'groupSize.second': 'n_disease', 'groupLinkDegree.first': 'd_drug',
                 'groupLinkDegree.second': 'd_disease', 'p.value': 'p_binox', 'FDR': 'fdr_binox'}
    b_clean = binox_results.rename(columns=new_names)

    # calculate the z-score for BinoX crosstalk
    p_prime = b_clean.expectedLinks / np.amin(np.array([b_clean.n_drug * b_clean.n_disease, b_clean.d_drug,
                                                        b_clean.d_disease]), axis=0)
    std_estimated = np.sqrt(b_clean.expectedLinks * (1 - p_prime))
    b_clean['z_binox'] = (b_clean.sharedOriglinks - b_clean.expectedLinks) / std_estimated

    # calculate NEAT
    # print(net_size, b_clean.n_drug, b_clean.d_disease)
    hyper_mean = hypergeom.mean(net_size, b_clean.n_drug, b_clean.d_disease)
    hyper_std = hypergeom.std(net_size, b_clean.n_drug, b_clean.d_disease)
    b_clean['z_hyper'] = (b_clean.sharedOriglinks - hyper_mean) / hyper_std
    b_clean['hyper'] = hypergeom.sf(b_clean.sharedOriglinks - 1, net_size, b_clean.n_drug, b_clean.d_disease)
    b_clean['p_hyper'] = np.where(b_clean.hyper.isnull(), 1, b_clean.hyper)
    _, b_clean['fdr_hyper'], __, ___ = multi.multipletests(b_clean.p_hyper, method='fdr_bh')

    if not with_flags:
        b_clean.to_csv(binox_flag_file, index=False, sep='\t')
        return b_clean

    print(flag_file)
    flag_df = pd.read_csv(flag_file, header=0, sep='\t')
    binox_with_flags = b_clean.merge(flag_df, how='outer', on=['group', 'disease'])
    binox_with_flags.to_csv(binox_flag_file, index=False, sep='\t')
    return binox_with_flags


def binox_wrapper(parameters=None, binox_network_file='../data/processed/funcoup.binox', binox_randomized_file=None,
                  re_randomize=False, group_a_file=None, group_b_file=None, with_flags=True, net_size=None,
                  binox_output_file='../data/processed/network.binox.tsv',
                  flag_file='../data/processed/proximity.dat', overwrite=False):
    """
    Wrapper for running BinoX analysis of crosstalk between two groups of nodes
    :param flag_file: str(optional)
        benchmark file with flags
    :type net_size: int (optional)
        the number of genes in the network
    :param with_flags:
    :param overwrite: bool (optional)
        if True, run binox analysis even if an output file already exists
    :param parameters: dictionary (optional)
        {'cutoff': string, 'min_nodes': string, 'verbose': string}
        For details on parameters see: # https://bitbucket.org/sonnhammergroup/binox/wiki/Usage
    :param binox_network_file: string (optional)
        Path to the binox formatted network file
    :param binox_randomized_file: string (optional)
        Path to the binox randomized network file
    :param group_a_file: string (optional)
        Path to the group a file
    :param group_b_file: string (optional)
        Path to the group b file
    :param binox_output_file: string (optional)
        Path where to store the output
    :param re_randomize: bool (optional)
        If True randomize even if a randomized file is available
    :return: pd.DataFrame
        Call to the parse_binox_output function which retruns dataframe with results
    """
    if os.path.exists(binox_output_file) and not overwrite:
        return clean_binox_output(binox_result_file=binox_output_file, net_size=net_size, with_flags=with_flags,
                                  flag_file=flag_file, overwrite=overwrite)

    binox = 'binox/BinoX'
    if parameters is None:
        parameters = {'cutoff': '0.99'}

    if 'cutoff' not in parameters:
        parameters['cutoff'] = '0.99'

    if group_a_file is not None and group_b_file is not None:
        if not os.path.exists(binox_randomized_file) or re_randomize:
            print(f'{binox_randomized_file} does not exist or re-randomization is {re_randomize}.')
            random_call = [binox, '-c', parameters['cutoff'], '-n', binox_network_file, '-r', binox_randomized_file]
            response = subprocess.run(random_call, stdout=subprocess.PIPE)
            print(response.stdout.decode('utf-8'))

    if 'min_nodes' not in parameters:
        parameters['min_nodes'] = '1'
    if 'verbose' not in parameters:
        parameters['verbose'] = 'large'

    call = [binox, '-c', parameters['cutoff'], '-g', parameters['min_nodes'], '-p', parameters['verbose'],
            '-r', binox_randomized_file, '-a', group_a_file, '-b', group_b_file, '-o', binox_output_file]
    if parameters is not None:
        call += parameters
    response = subprocess.run(call, stdout=subprocess.PIPE)
    print(response.stdout.decode('utf-8'))

    return clean_binox_output(binox_result_file=binox_output_file, net_size=net_size, with_flags=with_flags,
                              flag_file=flag_file, overwrite=overwrite)
