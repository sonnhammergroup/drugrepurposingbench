/*************************************************************
 *                   Christoph Ogris                         *
 *             christoph.ogris@scilifelab.se                 *
 *************************************************************/

#include "binresult.h"
#include "utils/uio.h"

bool BINResult::write(vector<string> &obj, string type){

    if( type == "compactHEADER" ||  type == "largeHEADER")
    {
        obj.push_back("#1:ID");
        obj.push_back("#2:NameGroupA");
        obj.push_back("#3:NameGroupB");
        obj.push_back("#4:p.value");
        obj.push_back("#5:FDR");
        obj.push_back("#6:relationType");
        obj.push_back("#7:PFC");
        if( type == "largeHEADER")
        {
            obj.push_back("#8:id.first");
            obj.push_back("#9:id.second");
            obj.push_back("#10:groupSize.first");
            obj.push_back("#11:groupSize.second");
            obj.push_back("#12:groupLinkDegree.first");
            obj.push_back("#13:groupLinkDegree.second");
            obj.push_back("#14:expectedLinks");
            obj.push_back("#15:sharedOriglinks");
            obj.push_back("#16:sharedRndlinks");
            obj.push_back("#17:randMax");
        }

        return true;
    }


    if((binomial != binomial) && (binomialFDR != binomialFDR) )
        return false;

    if( type == "compact" ||  type == "large")
    {
        obj.push_back(to_string(key));
        obj.push_back(Name.first);
        obj.push_back(Name.second);
        obj.push_back(to_string(binomial));
        obj.push_back(to_string(binomialFDR));
        obj.push_back(relationType);
        obj.push_back(to_string(PFC));
        if( type == "large")
        {
            obj.push_back(to_string(id.first));
            obj.push_back(to_string(id.second));
            obj.push_back(to_string(groupSize.first));
            obj.push_back(to_string(groupSize.second));
            obj.push_back(to_string(groupLinkDegree.first));
            obj.push_back(to_string(groupLinkDegree.second));
            obj.push_back(to_string(expectedLinks));
            obj.push_back(to_string(sharedOriglinks));
            obj.push_back(to_string(sharedRndlinks));
            obj.push_back(to_string(randMax));
        }

        return true;
    }


    return false;
}
