/*************************************************************
 *                   Christoph Ogris                         *
 *             christoph.ogris@scilifelab.se                 *
 *************************************************************/

#ifndef BINRESULT_H
#define BINRESULT_H

#include "binbase.h"

class BINResult : public BINBase
{
public:

    BINResult(int key,
              std::pair<std::string, string> name,
               pair<int, int> id,
               pair<int, int> groupSize,
               pair<int, int> groupLinkDegree,
               int sharedOriglinks,
               int sharedRndlinks,
               long double expectedLinks,
               long double pfc,
               int randMax):key(key),
                          Name(name),
                          id(id),
                          groupSize(groupSize),
                          groupLinkDegree(groupLinkDegree),
                          sharedOriglinks(sharedOriglinks),
                          sharedRndlinks(sharedRndlinks),
                          expectedLinks(expectedLinks),
                          PFC(pfc),
                          randMax(randMax){};


    int key;

    pair<std::string, string> Name;
    pair<int, int> id;
    pair<int, int> groupSize;
    pair<int, int> groupLinkDegree;

    int sharedOriglinks;
    int sharedRndlinks;

    long double expectedLinks;
    long double binomial;
    long double binomialFDR;
    string relationType;                  // [+] if enriched [-]if depleted
    long double PFC;
    int randMax;

    bool write(vector<string> &obj, string type);

};

#endif // BINRESULT_H
