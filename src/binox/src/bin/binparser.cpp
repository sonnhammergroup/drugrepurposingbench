/*************************************************************
 *                   Christoph Ogris                         *
 *             christoph.ogris@scilifelab.se                 *
 *************************************************************/


#include "binparser.h"

#include "bio/bgene.h"
#include <boost/algorithm/string.hpp>

bool parseNetwork(vector<vector<string> > &data,   pair<Network ,Network> &Networks,    BComposite &Genome, networkFile fileParam , double cuttOff){

    map<BGene*, Network::Node> geneVertMap;
    LinkProperties link;
    NodeProperties vp;
    Network::Node v1, v2;

    string geneNameA, geneNameB;
    BGene* A;
    BGene* B;

    bool hasA, hasB;

    int fileSize = data.size();
    double score = 0;
    double pfc = 0;

    if( (data.at(0).at(0)).at(0) == '#' )
            fileParam.io.header = true;
    else
            fileParam.io.header = false;



    for (int row = 0 ; row < fileSize; row++) {

        if(row == 0 && fileParam.io.header)
            continue;

        geneNameA = data.at(row).at(fileParam.colGeneA);
        geneNameB = data.at(row).at(fileParam.colGeneB);

        if( data.at(row).size() > 2 )
        {
            score = atof(data.at(row).at(fileParam.colLinkWeight).c_str());
            pfc = atof(data.at(row).at(fileParam.colLinkWeight).c_str());
        }else{
            score = 0;
            pfc = 0;

        }

// checking if link is smaller cuttoff. example if cuttoff is set to 0.75 ->  >= 0.75 will be included
        if (!geneNameA.size() || !geneNameB.size() || ((double)score < (double)cuttOff) || data.at(row).size() < fileParam.colSum)
            continue;

        boost::to_upper(geneNameA);
        boost::to_upper(geneNameB);

        BGene* geneA = new BGene(geneNameA);
        BGene* geneB = new BGene(geneNameB);

        hasA = true;
        hasB = true;

        if(!(BGene*)Genome.has(geneA)){
            hasA = false;
            Genome.add(new BGene(geneNameA));
        }

        if(!(BGene*)Genome.has(geneB)){
            hasB = false;
            Genome.add(new BGene(geneNameB));
        }

       A = (BGene*)Genome.get(geneA);
       B = (BGene*)Genome.get(geneB);

       if (!hasA && !hasB)
       {

           vp.gene = A;
           v1 = Networks.first.AddNode(vp);
           geneVertMap[vp.gene]= v1;

           vp.gene = B;
           v2 = Networks.first.AddNode(vp);
           geneVertMap[vp.gene]= v2;

           link.weight = score;
           link.pfc = pfc;
           Networks.first.AddLink(v1, v2, link, link);

      }
       else if(hasA && !hasB)
       {

           vp.gene = B;
           v2 = Networks.first.AddNode(vp);
           geneVertMap[vp.gene]= v2;

           v1 = geneVertMap[A];


           link.weight = score;
           link.pfc = pfc;
           Networks.first.AddLink(v1, v2, link, link);

       }
       else if (!hasA && hasB)
       {

           vp.gene = A;
           v1 = Networks.first.AddNode(vp);
           geneVertMap[vp.gene]= v1;

           v2 = geneVertMap[B];


           link.weight = score;
           link.pfc = pfc;
           Networks.first.AddLink(v1, v2, link, link);


       }
       else
       {

           v1 = geneVertMap[A];
           v2 = geneVertMap[B];


           link.weight = score;
           link.pfc = pfc;
           Networks.first.AddLink(v1, v2, link, link);

       }

       delete geneA;
       delete geneB;

    }

    Networks.second = Networks.first;

    Networks.first.id   = 0;
    Networks.second.id  = 1;

    return true;

}


bool parseGroups(vector<vector<string> > &data, BComposite &Container,BComposite *Genome, groupFile fileParam){


    string geneName, groupName;

    BComposite* group;

    if( (data.at(0).at(0)).at(0) == '#' )
            fileParam.io.header = true;
    else
            fileParam.io.header = false;

    int fileSize = data.size();
    for (int row = 0 ; row < fileSize; row++) {


        if(fileParam.io.header && row == 0)
            continue;

        geneName = data.at(row).at(fileParam.colGene);
        groupName = data.at(row).at(fileParam.colGroup);

        if (!geneName.size() || !groupName.size() || data.at(row).size() < fileParam.colSum || data.at(row).size() != 2 )
            continue;


        boost::to_upper(geneName);
        boost::to_upper(groupName);

        BGene* geneTmp = new BGene(geneName);
        BComposite* groupTmp = new BComposite(groupName);


        //if group exists get mem adress
        if(Container.has(groupTmp))
        {
            group = (BComposite*)Container.get(groupTmp);
            delete groupTmp;
        }
        else  //else add to container
        {
            Container.add(groupTmp);
            group = (BComposite*)Container.get(groupTmp);
        }

        // added genes from groups

        if(Genome->has(geneTmp))
        {
            group->add((BGene*)Genome->get(geneTmp));
            delete geneTmp;
        }


    }

    return true;
}


bool parseStResults(vector<vector<string> > &data, groupFile fileParam){


    string geneName;

    double score = 0;


    //file MUST contain header like:
    //#gene comp1   comp2   comp3   .....

    if( (data.at(0).at(0)).at(0) != '#' )
        return false;

    fileParam.io.header = true;
    fileParam.colSum = data.at(0).size();


    vector<string> pair;
    vector<vector<string> > tmp_data;

    int fileSize = data.size();
    for (int row = 0 ; row < fileSize; row++) {

        if(fileParam.io.header && row == 0)
            continue;

        geneName = data.at(row).at(fileParam.colGene);


        if( data.at(row).size() != fileParam.colSum){
            cout <<   "missing values" << endl;
            return false;
        }

        for (unsigned int col = 1 ; col < fileParam.colSum; col++) {
            score = atof(data.at(row).at(col).c_str());
            if(score >= fileParam.stProp.cutOff){

                pair.push_back(geneName);
                pair.push_back(data.at(0).at(col));
                tmp_data.push_back(pair);
                pair.clear();
            }
        }
    }
    data = tmp_data;

    tmp_data.clear();
    return true;
}


bool parseRandomNetwork(vector<vector<string> > &data, BComposite &Genome,  BinoXConfig &config){

    unsigned int fileSize = data.size();

    BGene* A;
    BGene* B;

    string geneNameA, geneNameB;

    double score = 0.0;
    int rnd = 0;
    int orig = 0;

    int unsigned id = 0;

    for (unsigned int row = 0; row < fileSize; row++)
    {
        if( (data.at(row).at(0)) == "#")
        {
            if (data.at(row).at(1)=="BinoX-Run")
                config.process.date = data.at(row).at(2);

            if (data.at(row).at(1)=="Network")
                config.fileFormatNetwork.io.fileName = data.at(row).at(2);

            if (data.at(row).at(1)=="Iterations")
                config.process.iterations = (unsigned int)atof(data.at(row).at(2).c_str());

            if (data.at(row).at(1)=="Method")
                config.process.method = data.at(row).at(2);

            if (data.at(row).at(1)=="Genes"){
                config.process.nodeCount = (unsigned int)atof(data.at(row).at(2).c_str());
            }

            if (data.at(row).at(1)=="Links")
                config.process.linkCount = (unsigned int)atof(data.at(row).at(2).c_str());

            if (data.at(row).at(1)=="Cuttoff")
            {
                config.process.cutOff = (double)atof(data.at(row).at(2).c_str());
            }
            continue;
        }

        geneNameA = data.at(row).at(0);
        geneNameB = data.at(row).at(1);

        rnd = atof(data.at(row).at(2).c_str());
        orig = atof(data.at(row).at(3).c_str());
        score = atof(data.at(row).at(4).c_str());


        BGene* geneA = new BGene(geneNameA);
        BGene* geneB = new BGene(geneNameB);

        if(!(BGene*)Genome.has(geneA))
        {
            Genome.add(new BGene(geneNameA));
            ((BGene*)Genome.get(geneA))->initRelGeneGene(config.process.nodeCount);
            ((BGene*)Genome.get(geneA))->setID(id);
            id+=1;

        }

        if(!(BGene*)Genome.has(geneB))
        {
            Genome.add(new BGene(geneNameB));
            ((BGene*)Genome.get(geneB))->initRelGeneGene(config.process.nodeCount);
            ((BGene*)Genome.get(geneB))->setID(id);
            id+=1;

        }

       A = (BGene*)Genome.get(geneA);
       B = (BGene*)Genome.get(geneB);

       A->relGeneGene->setPFC(B->getId(),score);
       A->relGeneGene->setOrigLink(B->getId() ,orig);
       A->relGeneGene->setRndLink(B->getId(),rnd);

       B->relGeneGene->setPFC(A->getId(),score);
       B->relGeneGene->setOrigLink(A->getId() ,orig);
       B->relGeneGene->setRndLink(A->getId(),rnd);

       delete geneA;
       delete geneB;


    }

    return true;
}

