/*************************************************************
 *                   Christoph Ogris                         *
 *             christoph.ogris@scilifelab.se                 *
 *************************************************************/

#ifndef BINANALYSE_H
#define BINANALYSE_H

#include "binbase.h"
#include "bio/bcomposite.h"
#include "graph/gtypes.h"
#include "binresult.h"

class BINAnalyse : public BINBase
{
private:
    /* BINAnalyse includes all functions Crosstalk needs.
     *
     * All genes will be loaded once and store all of the information. Pointer to those will be sored in the Genome object.
     * Most other objects will just be pointers to the genes. Exceptions are: Configurations & Results
     *
     */

    BComposite m_genome;                        // Genome, all available genes are stored here

    pair<BComposite,BComposite> m_group;        // group A and B
    set<int> m_uniqueGroupGenes;                // unique set of Genes in A and B

    pair<Network, Network> m_networks;          // ORIGINAL (first) and RANDOM (second) network

    map<int, vector<Record> > m_degRecordsMap;  // Deg Map for randomizations

    vector<BINResult*> m_Result;                // Results



public:
    BINAnalyse();
    clock_t timer;

    int links;
    int genes;

    float computationTime();


    /* loadNetwork
     *
     * Arguments:
     * 1 - [FILEPATH] to tab seperated (TSV) network file
     * 2 - [CUTTOFF]-Score for links in Network
     *
     * Function:
     * This function loads the networkfile into the memory and prepares the date for optimal performance.
     *
     * Sidenote:
     * ONLY links with a higher score than the user-defined cuttoff will be taken into account.
     *
     */
    bool loadNetwork(networkFile network, double cuttOff);




    bool loadRandomNet(BinoXConfig &conf);


    /*loadGroups
     *
     * Arguments:
     * 1 - FILEPATH to TSV groupA file
     * 2 - FILEPATH to TSV groupB file
     *
     * Function:
     * Loads all groups and their genes to the memory.
     *
     * Sidenote:
     * ONLY genes in the Network will be loaded into the memory.
     *
     */
    bool loadGroups(groupFile fileFormatGroupA, groupFile fileFormatGroupB);

    /*randomize
     *
     * Arguments:
     *
     * 1 - RANDOMIZATION METHOD. available: SecondOrder, LinkSwap, LabelSwap & Assignment
     * 2 - NUMBER OF RANTOMIZATIONS. How many random networks will be generated
     *
     * Function:
     * Randomizes the Network and saves the random link count
     *
     */
    bool randomize(processSetting config);

    /*prepareResultTable
     *
     * Arguments:
     *
     * 1 - MIN NUMBER OF GENES IN GROUP
     *
     * Function:
     * Generates all plausible combinations between the GroupFile A and GroupFile B.
     *
     * SideNote:
     * ONLY groups with more than MIN NUMBER OF GENES IN GROUP will be taken into account.
     *
     */
    bool prepareResultTable(processSetting config);

    /* calculateStatistics
     *
     * Arguments:
     *
     * Function:
     * Calculates binomial cdf and hypergeometric cdf of the Result combination.
     *
     * SideNote:
     * ONLY calculates upper tail.
     *
     */
    bool calculateStatistics(processSetting config);

    /*writeResult
     *
     * Arguments:
     *
     * 1 - PATH TO RESULT FILE
     * 2 - REPLACE RESULTS
     *
     * Function:
     * Saves the result.
     *
     *
     */
    bool writeResult(resultFile fileFormatResult);

    /*writeDataBase
     *
     * Arguments:
     *
     * 1 - PATH TO DATABASE
     * 2 - REPLACE DATABASE
     *
     * Function:
     * Saves the random network.
     *
     *
     */
    bool writeRandomNet(BinoXConfig conf);

};




#endif // BINANALYSE_H
