/*************************************************************
 *                   Christoph Ogris                         *
 *             christoph.ogris@scilifelab.se                 *
 *************************************************************/

#ifndef BINPARSER_H
#define BINPARSER_H

#include "utils/utypes.h"


#include "bio/bcomposite.h"
#include "graph/gtypes.h"


bool parseNetwork(vector<vector<string> > &data,   pair<Network, Network> &Networks,    BComposite &Genome, networkFile fileParam, double cuttOff);

bool parseGroups(vector<vector<string> > &data, BComposite &Container, BComposite *Genome, groupFile fileParam);

bool parseStResults(vector<vector<string> > &data, groupFile fileParam);

bool parseRandomNetwork(vector<vector<string> > &data, BComposite &Genome,  BinoXConfig &config);



#endif // BINPARSER_H
