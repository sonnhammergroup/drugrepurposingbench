set(bin_source_files
binbase.h
binanalyse.cpp
binparser.cpp
bintools.cpp
binresult.cpp
)
add_library(bin ${bin_source_files})
