/*************************************************************
 *                   Christoph Ogris                         *
 *             christoph.ogris@scilifelab.se                 *
 *************************************************************/



//-------------------------binanalyse.cpp--------------------------//

#include "binanalyse.h"

#include "bintools.h"
#include "binparser.h"

#include "bio/bgene.h"

#include "utils/uio.h"
#include "utils/ucalc.h"

#include "graph/grandomize.h"

#include "utils/uprint.h"
#include <time.h>

#include <cmath>

#include <exception>
#include <cstdio>
#include <iomanip>


// Constructor, starts timer
BINAnalyse::BINAnalyse()
{
    timer = clock();
}


/*************************************************************
 *                       LOAD NETWORK                        *
 *************************************************************/
bool BINAnalyse::loadNetwork(networkFile network, double cuttOff)
{
  // clock_t start = clock();

   // print filename and cuttoff
   printNetworkINFO(network.io.fileName,cuttOff);
   cout.flush();


   vector<vector<string> > data;
   data.clear();

    // config variables [path] and [sep] needed for reading network files
    if(!readFile(   (network.io.absolutePath + network.io.fileName), network.io.sep,    data))
        return false;

    // fetching genomic information according to the predefined columns
    if(!parseNetwork(data,    m_networks,    m_genome,  network, cuttOff))
        return false;

    // generate a deg map. Degree = outgoing links
    if(!generateDegreeMap(m_networks, m_degRecordsMap))
        return false;

    // generate a matrix how genes relate to each other. (geneA...geneM) x (geneA...geneM)
    if(!generateGeneRealtionMatrix(&m_genome, m_networks))
        return false;

   // determine number of loaded Links
   links = m_networks.first.getLinkCount();
   genes = m_networks.first.getNodeCount();

   // check if any link exists at all.
   if(links==0)
       return false;

   // print stats about loaded network
   printNetworkStatistics(genes,links);

 //  if(DEBUG)
  //      printTime("",(float)(clock() - start) / CLOCKS_PER_SEC);

   data.clear();
   return true;
}


/*************************************************************
 *                LOAD RANDOM NETWORK                        *
 *************************************************************/
bool BINAnalyse::loadRandomNet(BinoXConfig &conf)
{

    vector<vector<string> > networkData;

    if(!readFile(  (conf.filePreProcessedNetwork.absolutePath + conf.filePreProcessedNetwork.fileName) ,  conf.filePreProcessedNetwork.sep,    networkData))
        return false;


    if(!parseRandomNetwork(networkData,    m_genome,  conf))
        return false;

    printRandomNetConfig(conf);


    return true;
}




/*************************************************************
 *                  RANDOMIZE NETWORK                        *
 *************************************************************/
bool BINAnalyse::randomize(processSetting config)
{
    clock_t start = clock();

    printConfigurations(config.method, config.iterations);

    config.failedRandomizations = 0;

    for (unsigned int i = 0; i <  config.iterations; i++)
    {
        if(!generateRandomNetworkFile(config.method, m_networks , m_degRecordsMap))
        {
            i = i-1;
            config.failedRandomizations = config.failedRandomizations + 1;
            cout << "ERROR in randomization " << i << " - repeating process.\n This will increase the expected runtime." << endl;
        }

        if((config.failedRandomizations >= (config.iterations/2)) & (config.failedRandomizations != 0)){
            printf("***Warning*** Randomization failed to conserve connectivities in %d cases\n", config.failedRandomizations);
            return false;
        }


        Network::Node v1, v2;
        for (Network::link_range_t er = m_networks.second.getLinks(); er.first != er.second; er.first++)
        {
             m_networks.second.getNodesByLink(*er.first, v1, v2);

            BGene* A =  m_networks.second.properties(v1).gene;
            BGene* B =  m_networks.second.properties(v2).gene;

            A->relGeneGene->setRndLink(B->getId(),1);
            B->relGeneGene->setRndLink(A->getId(),1);
        }

        if(i==0)
        {
            printTime("Expected run time",((((float)(clock() - start) / CLOCKS_PER_SEC)) * (float)config.iterations));
            cout.flush();
        }

    }


 //   if(DEBUG)
  //      printTime("randomize",(float)(clock() - start) / CLOCKS_PER_SEC);
    return true;
}




/*************************************************************
 *                   LOAD GENE GROUPS                        *
 *************************************************************/
bool BINAnalyse::loadGroups(groupFile fileFormatGroupA, groupFile fileFormatGroupB)
{

   vector<vector<string> > data;
   data.clear();

   // Load and parse GROUP A
    if(!readFile(   (fileFormatGroupA.io.absolutePath + fileFormatGroupA.io.fileName) ,  fileFormatGroupA.io.sep,    data))
        return false;

    if(!parseGroups(data,    m_group.first,    &m_genome, fileFormatGroupA))
        return false;

    data.clear();



    // Load and parse GROUP B
    if(!readFile(   (fileFormatGroupB.io.absolutePath + fileFormatGroupB.io.fileName),  fileFormatGroupB.io.sep,    data))
        return false;


    // use parser for ST-result
    if(fileFormatGroupB.isStFile){
        if(!parseStResults(data, fileFormatGroupB))
            return false;
    }


    if(!parseGroups(data,    m_group.second,    &m_genome, fileFormatGroupB))
           return false;


    data.clear();

    if(!generateCompositeRelationMatrix(m_group))
        return false;

    printGeneSetStatistics(fileFormatGroupA.io.fileName,m_group.first.size(),fileFormatGroupB.io.fileName,m_group.second.size());

    return true;
}




/*************************************************************
 *                   PREPARE RESULTMATRIX                    *
 *************************************************************/
bool BINAnalyse::prepareResultTable(processSetting config)
{
    //clock_t start = clock();
    cout    << endl;
    cout    <<  "STATISTICAL ANALYSIS:"    <<  endl;
    cout    << setw(20) << config.minSetSize << setw(30) <<" Min. # of genes in group" <<endl;


    //evaluate relations between groups in the network and for hyper geometric testing
    if(!getGroupRelations(m_group,m_uniqueGroupGenes,config.minSetSize))
        return false;


    BComposite* geneSetA;
    BComposite* geneSetB;

    string tmp;
    set< string > NameList;
    pair<set< string >,set< string > > DiscardedGroupList;

    pair<string, string> name;
    pair<int, int> id, groupSize, groupLinkDegree;
    int origLinks, randLinks,key, randMax;
    long double expLinks , pfc;


    key = 0;

    for (BComposite::iterator iterA = m_group.first.begin(); iterA != m_group.first.end(); ++iterA)
    {
        geneSetA=((BComposite*)(*iterA));
        if(geneSetA->size() <  config.minSetSize)
        {
            DiscardedGroupList.first.insert(geneSetA->getName());
            continue;
        }

        for (BComposite::iterator iterB = m_group.second.begin(); iterB != m_group.second.end(); ++iterB)
        {
            geneSetB=((BComposite*)(*iterB));
            if(geneSetB->size() <  config.minSetSize)
            {
                DiscardedGroupList.second.insert(geneSetB->getName());
                continue;
            }

            name = make_pair("","");
            id = make_pair(0,0);
            groupSize = make_pair(0,0);
            groupLinkDegree = make_pair(0,0);
            origLinks = 0;
            randLinks = 0;
            randMax = 0;
            expLinks = 0.0;
            pfc = 0.0;

            key += 1;

            name = make_pair(geneSetA->getName(),geneSetB->getName());

            if(config.intra){
                tmp = geneSetA->getName() + geneSetB->getName();

                if (NameList.count(tmp)) {
                   continue;
                } else {
                    NameList.insert(tmp);
                    tmp = geneSetB->getName() + geneSetA->getName();
                    NameList.insert(tmp);
               }
            }


            id = make_pair(geneSetA->getId(),geneSetB->getId());
            groupSize = make_pair(geneSetA->size(),geneSetB->size());

            origLinks = geneSetA->relCompositeComposite->getOrigLink(id.second);
            randLinks = geneSetA->relCompositeComposite->getRndLink(id.second);
            randMax = geneSetA->relCompositeComposite->getRndMax(id.second);

            if( origLinks > 0)
                pfc = geneSetA->relCompositeComposite->getPFC(id.second) / ((long double)origLinks);

            if(origLinks != geneSetB->relCompositeComposite->getOrigLink(id.first))
                return false;

            if(randLinks != geneSetB->relCompositeComposite->getRndLink(id.first))
                return false;

            if(randLinks > 0)
                expLinks = ((long double)randLinks)/((long double)config.iterations);

            groupLinkDegree = make_pair(geneSetA->degree,geneSetB->degree);

            BINResult* res = new BINResult(key,name,id,groupSize,groupLinkDegree,origLinks,randLinks,expLinks,pfc,randMax);

            m_Result.push_back(res);


        }
    }



    cout    << setw(20) <<  DiscardedGroupList.first.size() << setw(30) << "#Groups in Set A to small"<<  endl;
    cout    << setw(20) <<  DiscardedGroupList.second.size() << setw(30) << "#Groups in Set B to small"<<  endl;

    return true;
}



/*************************************************************
 *         DETERMINE AND EVALUATE RELATIONS                  *
 *************************************************************/
bool BINAnalyse::calculateStatistics(processSetting config)
{
    //clock_t start = clock();

    BINResult* val;

    int counterBinom = 0;

    vector<long double*> adjustBinomialPtr;

    for (unsigned int i = 0; i < m_Result.size(); i++)
    {
        int links_n = NAN;
        int links_k = NAN;
        long double links_mu = NAN;
        long double binomial = NAN;

        //load values from result matrix
        val = m_Result.at(i);

        // compute Binomial
        //get max number of connections
        links_n = fmin(val->groupLinkDegree.first, val->groupLinkDegree.second);
        links_n = fmin(links_n, (val->groupSize.first*val->groupSize.second));
       // links_n = fmin(links_n, val->randMax);

        //get connections in Network
        links_k = val->sharedOriglinks;

        //get random links
        links_mu = val->expectedLinks;


        bool success = false;
        if (links_k != 0 && links_mu != 0)
        {
            try {
                if (config.relationType == "+-")
                { // If we want to search for both enrichment and depletion
                    success = binomialCDF(links_n, links_k, links_mu, binomial);
                    if(links_mu<=links_k)
                        val->relationType = "+";
                    if(links_mu>links_k)
                        val->relationType = "-";
                } else if (config.relationType == "+")
                { // If we want to search for enrichment only
                    success = binomialCDF_single(links_n, links_k, links_mu, binomial, true);
                    val->relationType = "+";
                } else if (config.relationType == "-")
                { // If we want to search for depletion only
                    success = binomialCDF_single(links_n, links_k, links_mu, binomial, false);
                    val->relationType = "-";
                }
            } catch (std::exception &e)
            {
                success = false;
            }

        } else
        {
            // No links means no p-value (black magic)
            success = false;
        }

        if (success)
        {
            val->binomial = binomial;
        } else
        {
            val->binomial = NAN;
        }





        // prepare vector with poiters for pvalue correction
        val->binomialFDR = binomial;
        adjustBinomialPtr.push_back(&val->binomialFDR);

        if(!(binomial!=binomial))
            counterBinom += 1;
    }



    cout    << setw(20) <<  counterBinom << setw(30) << "#tests for BinoX"<<  endl;

    // adjust pValues with Benjamini Hochberg Correction

    if(!pvalueAdjustBH(adjustBinomialPtr))
        return false;


    adjustBinomialPtr.clear();


  //  if(DEBUG)
  //      printTime("evaluate significance",(float)(clock() - start) / CLOCKS_PER_SEC);



    return true;
}




/*************************************************************
 *             WRITE EVALUATIONS TO FILES                    *
 *************************************************************/
bool BINAnalyse::writeResult(resultFile fileFormatResult)
{
    //clock_t start = clock();
    cout    << endl;
    cout    <<  "WRITING RESULTS:"    <<  endl;
    cout    << setw(50) << fileFormatResult.io.fileName <<  endl;


    vector<string> line;
    vector<vector<string> > data;


    //write header
    string tmp = fileFormatResult.fileFormat + "HEADER";
    if(!(*m_Result.at(0)).write(line, tmp))
        return false;
    tmp.clear();
    data.push_back(line);

    //write data
    if(m_Result.size() == 0)
        return false;

    for (unsigned int i = 0; i < m_Result.size(); i++)
    {
        line.clear();
        //if all results nan don't output the line
        if((*m_Result.at(i)).write(line, fileFormatResult.fileFormat))
                    data.push_back(line);

    }


    if(!writeFile(data,fileFormatResult.io))
        return false;


  //  if(DEBUG)
  //      printTime("write result",(float)(clock() - start) / CLOCKS_PER_SEC);
    return true;
}


/*************************************************************
 *             WRITE RANDOM NETWOR TO FILES                  *
 *************************************************************/

bool BINAnalyse::writeRandomNet(BinoXConfig conf)
{
    //clock_t start = clock();
    cout    << endl;
    cout    <<  "WRITING RANDOM NETWORKFILE:"    <<  endl;
    cout    << setw(50) << conf.filePreProcessedNetwork.fileName <<  endl;


    if(!generateRandomNetworkFile(conf,m_genome))
        return false;


  //  if(DEBUG)
  //      printTime("write random network",(float)(clock() - start) / CLOCKS_PER_SEC);
    return true;
}



float BINAnalyse::computationTime()
{
   return ((float)(clock() - timer) / CLOCKS_PER_SEC);
}
