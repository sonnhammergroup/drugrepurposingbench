/*************************************************************
 *                   Christoph Ogris                         *
 *             christoph.ogris@scilifelab.se                 *
 *************************************************************/

#include "bintools.h"

#include "bio/bgene.h"

#include "utils/uio.h"

#include <iomanip>

/*************************************************************
 *                GET GROUP RELATIONS                        *
 *************************************************************/
// fetches genomic data
bool getGroupRelations(pair<BComposite,BComposite> &composites,set<int> &uniqueGenes,unsigned int min)
{

    pair<int,int> links;
    double pfcs;
    int rndMax;
    BComposite* geneSetA;
    BComposite* geneSetB;
    BRelation* linksA;

    links = make_pair(0,0);
    rndMax = 0;
    pfcs = 0.0;

    //loob for GROUP A
    for (BComposite::iterator iterA = composites.first.begin(); iterA != composites.first.end(); ++iterA)
    {

        geneSetA=((BComposite*)(*iterA));
        if(geneSetA->size() <= min)             // if group A is to small skip it
            continue;

        //loob for GROUP A
        for (BComposite::iterator iterB = composites.second.begin(); iterB != composites.second.end(); ++iterB)
            {

                geneSetB=((BComposite*)(*iterB));
                if(geneSetB->size() <= min)     // if group B is to small skip it
                    continue;

                //iterate over genes in group A
                for (BComposite::iterator subIterA = geneSetA->begin(); subIterA != geneSetA->end(); ++subIterA)
                {
                    //iterate over genes in group B
                    for (BComposite::iterator subIterB = geneSetB->begin(); subIterB != geneSetB->end(); ++subIterB)
                    {
                        linksA = ((BGene*)(*subIterA))->relGeneGene; // relations representing all links in random and orig network [1,0,.....,#genes in network] 1 == link
                        links.first+=linksA->getOrigLink((*subIterB)->getId());
                        links.second+=linksA->getRndLink((*subIterB)->getId());
                        pfcs+=linksA->getPFC((*subIterB)->getId());
                        if(linksA->getRndLink((*subIterB)->getId()) != 0){
                          rndMax += 1;
                        }
                    }
                }

                geneSetA->relCompositeComposite->setOrigLink(geneSetB->getId(),links.first);
                geneSetA->relCompositeComposite->setRndLink(geneSetB->getId(),links.second);
                geneSetA->relCompositeComposite->setPFC(geneSetB->getId(),pfcs);
                geneSetA->relCompositeComposite->setRndMax(geneSetB->getId(),rndMax);


                geneSetB->relCompositeComposite->setOrigLink(geneSetA->getId(),links.first);
                geneSetB->relCompositeComposite->setRndLink(geneSetA->getId(),links.second);
                geneSetB->relCompositeComposite->setPFC(geneSetA->getId(),pfcs);
                geneSetB->relCompositeComposite->setRndMax(geneSetA->getId(),rndMax);



                // reset used variables
                links = make_pair(0,0);
                pfcs = 0.0;
                rndMax = 0;
            }
    }




    for (BComposite::iterator iterA = composites.first.begin(); iterA != composites.first.end(); ++iterA)
    {
        geneSetA=((BComposite*)(*iterA));
        if(geneSetA->size() <= min)
            continue;

        for (BComposite::iterator subIterA = geneSetA->begin(); subIterA != geneSetA->end(); ++subIterA)
        {
            uniqueGenes.insert(((BGene*)(*subIterA))->getId());
            geneSetA->degree += ((BGene*)(*subIterA))->relGeneGene->sumOrigLink();

        }

    }

    for (BComposite::iterator iterB = composites.second.begin(); iterB != composites.second.end(); ++iterB)
    {
        geneSetB=((BComposite*)(*iterB));
        if(geneSetB->size() <= min)
            continue;

        for (BComposite::iterator subIterB = geneSetB->begin(); subIterB != geneSetB->end(); ++subIterB)
        {
            uniqueGenes.insert(((BGene*)(*subIterB))->getId());
            geneSetB->degree += ((BGene*)(*subIterB))->relGeneGene->sumOrigLink();
        }

    }



    return true;
}


bool generateGeneRealtionMatrix(BComposite* Genome,pair<Network,Network> &networks)
{
    unsigned int size = Genome->size();
    unsigned int geneNr=0;

    for (BComposite::iterator it = Genome->begin(); it != Genome->end(); ++it) {
       ((BGene*)(*it))->setID(geneNr);
       ((BGene*)(*it))->relGeneGene = new BRelation(size);
       geneNr++;
    }

    if(geneNr != size)
        return false;



    Network::Node v1, v2;
    double pfc;
    for (Network::link_range_t er = networks.first.getLinks(); er.first != er.second; er.first++)
    {
        networks.first.getNodesByLink(*er.first, v1, v2);

        pfc= networks.first.properties(*er.first).pfc;

        BGene* A = networks.first.properties(v1).gene;
        BGene* B = networks.first.properties(v2).gene;

        A->relGeneGene->setOrigLink(B->getId(),1);
        B->relGeneGene->setOrigLink(A->getId(),1);

        A->relGeneGene->setPFC(B->getId(),pfc);
        B->relGeneGene->setPFC(A->getId(),pfc);
    }

    return true;
}

#define DEGREE_BIN(x)		((int)round(log(x)+1))

bool generateDegreeMap(pair<Network,Network> &networks, map<int, vector<Record> > &degRecordsMap)
{

    Network::Node v1, v2;
    map<BGene*, Network::Node> g2vMap;
    BGene* gene;

    map<BGene*, vector<Network::Node> > geneVertMap;
    degRecordsMap.clear();

    for (Network::node_range_t vr = networks.second.getNodes(); vr.first != vr.second; vr.first++)
    {
        v1 = *vr.first;
        g2vMap[networks.second.properties(v1).gene] = v1;

        Record record;
        record.node = v1;
        record.degree = networks.second.getNodeDegree(record.node);
        degRecordsMap[DEGREE_BIN(record.degree)].push_back(record);
    }
    for (Network::node_range_t vr = networks.first.getNodes(); vr.first != vr.second; vr.first++)
    {
        v1 = *vr.first;
        gene = networks.first.properties(v1).gene;
        geneVertMap[gene].push_back(v1);
        geneVertMap[gene].push_back(g2vMap[gene]);
    }

    for (Network::node_range_t vr = networks.second.getNodes(); vr.first != vr.second; vr.first++)
    {
        v1 = *vr.first;
        for (Network::adjacency_node_range_t avr = networks.second.getAdjacentNodes(v1); avr.first != avr.second; avr.first++)
            networks.first.properties(geneVertMap[networks.second.properties(v1).gene][0]).connectedDegrees.push_back(DEGREE_BIN(networks.second.getNodeDegree(*avr.first)));
    }

    for (Network::node_range_t vr = networks.first.getNodes(); vr.first != vr.second; vr.first++)
        networks.second.properties(geneVertMap[networks.first.properties(*vr.first).gene][1]).connectedDegrees = networks.first.properties(*vr.first).connectedDegrees;

    g2vMap.clear();

    return true;
}



bool generateCompositeRelationMatrix(pair<BComposite,BComposite> &groups)
{

    unsigned int groupNr, groupSize;

    groupSize = 0;
    groupNr = 0;
    groupSize = groups.second.size();
    for (BComposite::iterator iterA = groups.first.begin(); iterA != groups.first.end(); ++iterA)
    {
        ((BComposite*)(*iterA))->setID(groupNr);
        ((BComposite*)(*iterA))->relCompositeComposite= new BRelation(groupSize);
        groupNr++;
    }


    groupSize = 0;
    groupNr = 0;

    groupSize = groups.first.size();
    for (BComposite::iterator iterB =  groups.second.begin(); iterB !=  groups.second.end(); ++iterB)
    {
        ((BComposite*)(*iterB))->setID(groupNr);
        ((BComposite*)(*iterB))->relCompositeComposite= new BRelation(groupSize);
        groupNr++;
    }

    return true;
}






bool generateRandomNetworkFile(BinoXConfig conf, BComposite &genome)
{
    vector<string>geneMap, line; // GeneA \t GeneB \t (- if not in orig net /+ if in orig net)mean

    vector<vector<string> > file;

    //ugly hack2

    time_t     now = time(0);
      struct tm  tstruct;
      char       buf[80];
      tstruct = *localtime(&now);
      // Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
      // for more information about date/time format
      strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);




    line.push_back(to_string("#\tBinoX-Run"));
    line.push_back(to_string(   buf    ));
    line.push_back("");
    line.push_back("");
    line.push_back("");

    file.push_back(line);
    line.clear();

    line.push_back(to_string("#\tNetwork"));
    line.push_back(to_string(   conf.fileFormatNetwork.io.fileName  ));
    line.push_back("");
    line.push_back("");
    line.push_back("");
    file.push_back(line);
    line.clear();

    line.push_back(to_string("#\tGenes"));
    line.push_back(to_string(   conf.process.nodeCount    ));
    line.push_back("");
    line.push_back("");
    line.push_back("");
    file.push_back(line);
    line.clear();

    line.push_back(to_string("#\tLinks"));
    line.push_back(to_string(   conf.process.linkCount    ));
    line.push_back("");
    line.push_back("");
    line.push_back("");
    file.push_back(line);
    line.clear();

    line.push_back(to_string("#\tIterations"));
    line.push_back(to_string(   conf.process.iterations    ));
    line.push_back("");
    line.push_back("");
    line.push_back("");
    file.push_back(line);
    line.clear();

    line.push_back(to_string("#\tMethod"));
    line.push_back(to_string(   conf.process.method   ));
    line.push_back("");
    line.push_back("");
    line.push_back("");
    file.push_back(line);
    line.clear();

    line.push_back(to_string("#\tCuttoff"));
    line.push_back(to_string(   conf.process.cutOff  ));
    line.push_back("");
    line.push_back("");
    line.push_back("");
    file.push_back(line);
    line.clear();

    line.push_back(to_string("#\tGeneA"));
    line.push_back(to_string(   "GeneB"    ));
    line.push_back(to_string(   "randomLink"    ));
    line.push_back(to_string(   "originalLink"    ));
    line.push_back(to_string(   "pfcScore"    ));
    file.push_back(line);
    line.clear();



    for (BComposite::iterator iter = genome.begin(); iter != genome.end(); ++iter)
    {
        geneMap.push_back((*iter)->getName());

        if(geneMap.size()-1 != (*iter)->getId())
        {
           cout << "Sorry, Somthing went wrong" << endl;
           exit(0);
        }
    }

    BGene* geneA;


    for (BComposite::iterator iter = genome.begin(); iter != genome.end(); ++iter)
    {
       geneA = (BGene*)(*iter);


       for (unsigned int i = geneA->getId(); i < genome.size(); i++)
       {

           //if it has random or orig link print

           if(geneA->relGeneGene->getRndLink(i) > 0 || geneA->relGeneGene->getOrigLink(i) > 0)
           {

                line.push_back(to_string(   geneA->getName()    ));
                line.push_back(to_string(   geneMap.at(i)    ));
                line.push_back(to_string(   geneA->relGeneGene->getRndLink(i)    ));
                line.push_back(to_string(   geneA->relGeneGene->getOrigLink(i)    ));
                line.push_back(to_string(   geneA->relGeneGene->getPFC(i)    ));

                file.push_back(line);
                line.clear();

           }



       }



    }


    if(!writeFile(file, conf.filePreProcessedNetwork))
        return false;
    return true;
}


void printNetworkINFO(string name, double cuttOff)
{
    cout    << endl;
    cout    << setw(10)<<  "LOADING NETWORK:  "  <<  endl;


    cout    << setw(20)<<   name   << setw(30)<<  "Networkfile"  <<  endl;

    cout    << setw(20)<<   cuttOff   << setw(30)<<  "Used threshold"  <<  endl;

    //cout    << "--------------------------------------------------" << endl;

}

void printNetworkStatistics(int genes, int links)
{
    cout    << setw(20)<<   links   << setw(30)<<  " Imported links"  <<  endl;
    cout    << setw(20)<<   genes   << setw(30)<<  " Imported genes"  <<  endl;
    //cout    << "--------------------------------------------------" << endl;

}

void printGeneSetStatistics(string nameA, int sizeA, string nameB, int sizeB)
{

    cout    << endl;
    cout    << setw(10)<<  "GENESET A:"<< setw(10)<< sizeA     << setw(30) <<  nameA << endl;
    cout    << setw(10)<<  "GENESET B:"<< setw(10)<< sizeB     << setw(30) <<  nameB << endl;
    //cout    << "--------------------------------------------------" << endl;

}

void printConfigurations(string method, int iterations)
{
    cout    << endl;
    cout    <<  "RANDOMIZING NETWORK:"    <<  endl;
    cout    << setw(20) << method<< setw(30) <<" Method" <<endl;
    cout    << setw(20) << iterations<< setw(30)<<" Randomizations" <<endl;
}

void printRandomNetConfig(BinoXConfig config)
{
    cout    << endl;
    cout    <<  "PREPROCESSED NETWORK:"    <<  endl;
    cout    << setw(20) << config.process.date          <<  setw(30)    <<" Build date"         <<endl;
    cout    << setw(20) << config.fileFormatNetwork.io.fileName          <<  setw(30)    <<" Filename"         <<endl;
    cout    << setw(20) << config.process.method        <<  setw(30)    <<" Method"         <<endl;
    cout    << setw(20) << config.process.iterations    <<  setw(30)    <<" #Randomizations" <<endl;
    cout    << setw(20) << config.process.nodeCount          <<  setw(30)    <<" #Genes"         <<endl;
    cout    << setw(20) << config.process.linkCount         <<  setw(30)    <<" #Links"         <<endl;
}
