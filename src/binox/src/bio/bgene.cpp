/*************************************************************
 *                   Christoph Ogris                         *
 *             christoph.ogris@scilifelab.se                 *
 *************************************************************/

#include "bgene.h"

BGene::BGene(string name)
{
    m_Name = name;
}

void BGene::initRelGeneGene(unsigned int size)
{
    relGeneGene = new BRelation(size);
}

