#include "btools.h"

bool fillCompositeRelations(pair<BComposite,BComposite> &composites,unsigned int min)
{

    pair<int,int> links;
    BComposite* geneSetA;
    BComposite* geneSetB;
    BRelation* linksA;

    for (BComposite::iterator iterA = composites.first.begin(); iterA != composites.first.end(); ++iterA)
    {

        geneSetA=((BComposite*)(*iterA));
        if(geneSetA->size() <= min)
            continue;

        for (BComposite::iterator iterB = composites.second.begin(); iterB != m_group.second.end(); ++iterB)
            {

                geneSetB=((BComposite*)(*iterB));
                if(geneSetB->size() <= min)
                    continue;


                for (BComposite::iterator subIterA = geneSetA->begin(); subIterA != geneSetA->end(); ++subIterA)
                {
                    linksA = ((BGene*)(*subIterA))->relGeneGene;
                    for (BComposite::iterator subIterB = geneSetB->begin(); subIterB != geneSetB->end(); ++subIterB)
                    {

                        links.first+=linksA->getOrigLink((*subIterB)->getId());
                        links.second+=linksA->getRndLink((*subIterB)->getId());

                    }
                }

                geneSetA->relCompositeComposite->setOrigLink(geneSetB->getId(),links.first);
                geneSetA->relCompositeComposite->setRndLink(geneSetB->getId(),links.second);

                geneSetB->relCompositeComposite->setOrigLink(geneSetA->getId(),links.first);
                geneSetB->relCompositeComposite->setRndLink(geneSetA->getId(),links.second);

            }
    }

    return true;
}
