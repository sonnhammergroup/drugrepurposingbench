/*************************************************************
 *                   Christoph Ogris                         *
 *             christoph.ogris@scilifelab.se                 *
 *************************************************************/

#include "bcomposite.h"


BComposite::BComposite()
{
}

BComposite::BComposite(std::string name)
{
    m_Name = name;
    degree = 0;
}


void BComposite::add(BBase* obj)
{  
     m_container.insert(obj);
}

void BComposite::clear()
{
    m_container.clear();
}

BBase* BComposite::get(BBase* obj)
{
  return *m_container.find(obj);
}

bool BComposite::has(BBase* obj)
{
    return (m_container.find(obj) != m_container.end());
}

unsigned int BComposite::size(){
    return m_container.size();
}

void BComposite::initRelCompositeComposite(unsigned int size)
{
    relCompositeComposite = new BRelation(size);
}


