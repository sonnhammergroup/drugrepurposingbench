/*************************************************************
 *                   Christoph Ogris                         *
 *             christoph.ogris@scilifelab.se                 *
 *************************************************************/

#ifndef BCOMPOSITE_H
#define BCOMPOSITE_H



#include "bbase.h"
#include "brelation.h"


class BComposite : public BBase
{
private:
    std::set<BBase*,BBase::cmp>  m_container;


public:
    BComposite();
    BComposite(string name);

    int degree;

    void add(BBase* obj);
    void clear();

    BBase *get(BBase* obj);
    bool has(BBase* obj);

    unsigned int size();

    BRelation* relCompositeComposite;
    void initRelCompositeComposite(unsigned int size);

    typedef std::set<BBase*,BBase::cmp>::iterator iterator;
    iterator begin() { return m_container.begin();}
    iterator end() { return m_container.end();}
};

#endif // BCOMPOSITE_H
