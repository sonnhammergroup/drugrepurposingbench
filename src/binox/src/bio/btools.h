#ifndef BTOOLS_H
#define BTOOLS_H

#include "bcomposite.h"
#include "bgene.h"
bool fillGroupRelations(pair<BComposite,BComposite> composites,int min);


#endif // BTOOLS_H
