#include "bparse.h"

#include <boost/algorithm/string.hpp>

bool parseGroups(vector<vector<string> > &data, BComposite &Container,BComposite *Genome, fileGroup fileParam){

    string geneName, groupName;

    BComposite* group;

    int fileSize = data.size();
    for (int row = 0 ; row < fileSize; row++) {

        if(fileParam.header && row == 0)
            continue;

        geneName = data.at(row).at(fileParam.colGene);
        groupName = data.at(row).at(fileParam.colGroup);

        if (!geneName.size() || !groupName.size() || data.at(row).size() < fileParam.colSum)
            continue;

        boost::to_upper(geneName);
        boost::to_upper(groupName);

        BGene* geneTmp = new BGene(geneName);
        BComposite* groupTmp = new BComposite(groupName);


        if(Container.has(groupTmp))
        {
            group = (BComposite*)Container.get(groupTmp);
        }
        else
        {
            Container.add(groupTmp);
            group = (BComposite*)Container.get(groupTmp);
        }

        if(Genome->has(geneTmp))
        {
            group->add((BGene*)Genome->get(geneTmp));

            //group->addDegree( gene->countOrigDegree());
            //Genome->addID(gene->getGeneRankID());
        }
        //else
            //group->addMiss();




    }

    return true;
}
