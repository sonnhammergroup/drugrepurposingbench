/*************************************************************
 *                   Christoph Ogris                         *
 *             christoph.ogris@scilifelab.se                 *
 *************************************************************/

#ifndef BRELATION_H
#define BRELATION_H

#include "bbase.h"

class BRelation : public BBase
{
private:
    vector<int> m_random;
    vector<int> m_orig;
    vector<int> m_rndMax;
    vector<double> m_origPFC;
    unsigned int m_size;
public:
    BRelation(unsigned int);

    unsigned int size();

    void setRndLink(unsigned int pos, int a);
    int getRndLink(unsigned int pos);
    int sumRndLink();

    void setRndMax(unsigned int pos, int a);
    int getRndMax(unsigned int pos);

    void setOrigLink(unsigned int pos, int a);
    int getOrigLink(unsigned int pos);
    int sumOrigLink();


    void setPFC(unsigned int pos, double a);
    double getPFC(unsigned int pos);



};

#endif // BRELATION_H
