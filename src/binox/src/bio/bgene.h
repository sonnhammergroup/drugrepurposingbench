/*************************************************************
 *                   Christoph Ogris                         *
 *             christoph.ogris@scilifelab.se                 *
 *************************************************************/

#ifndef GENE_H
#define GENE_H

#include "bbase.h"
#include "brelation.h"

class BGene : public BBase
{
public:
    BGene(string name);
    virtual ~BGene(){};

    BRelation* relGeneGene;
    void initRelGeneGene(unsigned int size);


};

#endif // GENE_H
