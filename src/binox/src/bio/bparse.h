#ifndef BPARSE_H
#define BPARSE_H

#include "bbase.h"
#include "bcomposite.h"
#include "bgene.h"
#include "btypes.h"

bool parseGroups(vector<vector<string> > &data, BComposite &Container, BComposite *Genome, fileGroup fileParam);

#endif // BPARSE_H
