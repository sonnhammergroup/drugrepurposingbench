/*************************************************************
 *                   Christoph Ogris                         *
 *             christoph.ogris@scilifelab.se                 *
 *************************************************************/


/*  oooooooooo.   o8o                        ooooooo  ooooo
 *  `888'   `Y8b  `"'                         `8888    d8'
 *   888     888 oooo  ooo. .oo.    .ooooo.     Y888..8P
 *   888oooo888' `888  `888P"Y88b  d88' `88b     `8888'
 *   888    `88b  888   888   888  888   888    .8PY888.
 *   888    .88P  888   888   888  888   888   d8'  `888b
 *  o888bood8P'  o888o o888o o888o `Y8bod8P' o888o  o88888o
 *      =================================================
 *
 *
 * BinoX is a genome wide network analysis tool which is based
 * on a monte carlo approach and designed to identify significant
 * relations between and within sets of genes which is valuable for
 * functional annotation of experimental gene sets.
 *
 * BinoX has two major functions:
 *          1.) randomization of biological networks
 *          2.) statistical evaluation of subnetworks(groups) relations
 *
*/


#include <iostream>

#include "bin/binanalyse.h"     //  includes all computational methods
#include "utils/uargparser.h"   //  includes parser for user variables
#include "utils/uprint.h"       //  includes output messages


using namespace std;


#define VERSION "v0.9.6"


int main(int argc, char* argv[])
{

 //   try{

    BinoXConfig config;                                        // [config] stores all configuration variables.
    config.version = VERSION;

    config.DEBUG = false;                                           // [DEBUG] flag (partly included, if enabled only time will be displayed)


    if(!uInitArgParser(argc,argv, config))                          // parse input variable to [config]
        printErrorAndExit("ERROR: wrong input parameter");

    printBanner();                                                  // print BinoX Banner

    BINAnalyse BinoX;                                   // [BinoX] constructor for BinoX analysis

    /*************************************************************
     *              LOAD AND RANDOMIZE NETWORK                   *
     *************************************************************/

    if( config.newNetwork )
    {
        /* FILE CONFIGURATION SETTINGS   */
        config.fileFormatNetwork.colGeneA        =   0;          //  Column gene A
        config.fileFormatNetwork.colGeneB        =   1;          //  Column gene B
        config.fileFormatNetwork.colLinkWeight   =   2;          //  Column for link score. Score which is used if [cuttoff] is defined
        config.fileFormatNetwork.colSum          =   3;          //  Max number of columns. Just for safety to check network file consistency.
        config.fileFormatNetwork.io.header       =   true;       //  If TRUE, use first line as Header
        config.fileFormatNetwork.io.sep          =   "\t";       //  Column seperator


        //  LOAD NETWORK
        if(!BinoX.loadNetwork(config.fileFormatNetwork, config.process.cutOff))
            printErrorAndExit("failed loading networkfile");

        config.process.linkCount = BinoX.links;               //  Number of links in the original network
        config.process.nodeCount = BinoX.genes;               //  Number of links in the original network

        // config for random generator
        srand(config.process.seed);

        //  RANDOMIZE NETWORK
        if(!BinoX.randomize(config.process))
            printErrorAndExit("randomizing network failed");
    }

    /*************************************************************
     *                 LOAD RANDOMIZED NETWORK                   *
     *************************************************************/
    if(config.preProcessedNetwork)
    {
        /* FILE CONFIGURATION SETTINGS  */
        config.filePreProcessedNetwork.sep = "\t";               //  Column seperator
        config.filePreProcessedNetwork.header = true;            //  If TRUE, use first line as Header

        //  LOAD RANDOMIZED NETWORK
        if(!BinoX.loadRandomNet(config))
            printErrorAndExit("failed loading networkdata");

    }


    /*************************************************************
     *            EVALUATE STATISTICS OF GENE SETS               *
     *************************************************************/
    if(config.evaluateGroups)
    {
        /* FILE CONFIGURATION SETTINGS  */
        //  group A
        config.fileFormatGroupA.colGene     = 0;          //  Column of gene
        config.fileFormatGroupA.colGroup    = 1;          //  Column of group name
        config.fileFormatGroupA.colSum      = 2;          //  Max number of columns. Just for safety to check network file consistency.
        config.fileFormatGroupA.io.header      = true;      //  Use first line as Header
        config.fileFormatGroupA.io.sep         = "\t";       //  Column seperator
        //  group B
        config.fileFormatGroupB.colGene     = 0;          //  Column of gene
        config.fileFormatGroupB.colGroup    = 1;          //  Column of group name
        config.fileFormatGroupB.colSum      = 2;          //  Max number of columns. Just for safety to check network file consistency.
        config.fileFormatGroupB.io.header      = true;      //  Use first line as Header
        config.fileFormatGroupB.io.sep         = "\t";       //  Column seperator
        //  outputFile
        config.fileFormatResult.io.sep = "\t";                 //  Column seperator
        //config.fileFormatResult.extent = "compact";       //  Information content [compact] [large]

        // LOAD GROUP FILES
        if(!BinoX.loadGroups(config.fileFormatGroupA, config.fileFormatGroupB))
            printErrorAndExit("failed loading geneset data" );

        // FILTER UNINFORMATIVE COMBINATION
        if(!BinoX.prepareResultTable(config.process))
            printErrorAndExit("cannot compare genesets");

        // CALCULATE STATISTICS
        if(!BinoX.calculateStatistics(config.process))
            printErrorAndExit("statistical calculations failed");

        // WRITE RESULT FILE
        if(!BinoX.writeResult(config.fileFormatResult))
            printErrorAndExit("ERROR: writing results failed");
    }

    /*************************************************************
     *                WRITE RANDOM NETWORK FILE                  *
     *************************************************************/
    if(config.createPreProcessedNetwork)
    {
        /* FILE CONFIGURATION SETTINGS  */
       // config.fileRandomNet.process = config.process;                      //  Parameter of used randomization
       // config.fileRandomNet.process.originalNetworkFile = config.fileFormatNetwork.fileName;    //  Network filename
       // config.fileRandomNet.cuttoff = config.fileFormatNetwork.cuttoff;    //  Used threshold for linkselection


        //OUTPUT FILE
        config.filePreProcessedNetwork.sep = "\t";                             //  Column seperator

        if(!BinoX.writeRandomNet(config))
            printErrorAndExit("writing filesystem failed");

    }


    cout    << endl;
    printTime("Computation Time",BinoX.computationTime());


    return 0;
}
