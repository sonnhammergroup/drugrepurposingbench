/*************************************************************
 *                   Christoph Ogris                         *
 *             christoph.ogris@scilifelab.se                 *
 *************************************************************/

#ifndef GRANDOMIZE_H
#define GRANDOMIZE_H

#include "gtypes.h"


bool generateRandomNetworkFile(string method, pair <Network,Network> &networks , map<int, vector<Record> > &degRecordsMap);

/*  generateRandomNetworkSecondOrder:
*	Best effort randomization of the original network. Attempts to conserve second-order assortativity.
*
*   networks: pair of networks .firs = original net .second = random network
*   degRecordsMap: a map from degree bin to records with that degree bin (from generateMaps below)
*	returns true if randomization conserved connectivities of the original else false
*/

bool generateRandomNetworkSecondOrder(pair<Network, Network> &networks, map<int, vector<Record> > &degRecordsMap);

/*  generateRandomNetworkLinkSwap:
*	Swap links as suggested by maslov and sneppen: link pair (a, b) and (c, d) become
*	(a, c) and (b, d) or (a, d) and (c, b)
*
*   networks: pair of networks .firs = original net .second = random network
* 	returns the number of swaps performed
*/
bool generateRandomNetworkLinkSwap(pair <Network,Network> &networks );

/*  generateRandomNetworkLabelSwap:
*	Permutates node labels that fall into the same ln(deg) bin.
*
*   networks: pair of networks .firs = original net .second = random network
*   degRecordsMap: a map from degree bin to records with that degree bin (from generateMaps below)
*	returns true if randomization conserved connectivities of the original else false
*/
bool generateRandomNetworkLabelSwap(pair <Network,Network> &networks , map<int, vector<Record> > &degRecordsMap);


/*  generateRandomNetworkAssignment:
*	Best effort randomization of the original network.
*
*   networks: pair of networks .firs = original net .second = random network
*	returns true if randomization conserved connectivities of the original else false
*/
bool generateRandomNetworkAssignment(pair <Network,Network> &networks );

/*  validateConnectivities:
*	Checks the node connectivities of the randNet network against the connectivities of the origNet network.
*	Generates a list of errors sorted by size of differences in increasing order.
*
*   networks: pair of networks .firs = original net .second = random network
*	errors: a vector that afterwards contains pairs of vertices of origNet and randNet that
*			have different connectivities between origNet and randNet sorted from smallest difference to largest
*	returns true if randNet has the same connectivities as origNet else false
*/
bool validateConnectivities(pair<Network, Network> &networks,
                            vector<pair<Network::Node, Network::Node> > &errors);

/*  fixConnectivityErrors:
*	Fixes the node connectivities of the randNet network to match connectivities of the origNet network
*	using the errors found by validateConnectivities
*
*   networks: pair of networks .firs = original net .second = random network
*	errors: a vector containing pairs of vertices of origNet and randNet that
*			have different connectivities between origNet and randNet sorted from smallest difference to largest
*/
void fixConnectivityErrors(pair<Network, Network> &networks,
                           vector<pair<Network::Node, Network::Node> > &errors);


#endif // GRANDOMIZE_H
