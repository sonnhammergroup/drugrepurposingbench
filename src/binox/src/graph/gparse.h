#ifndef GPARSE_H
#define GPARSE_H

#include "bio/bgene.h"
#include "bio/bcomposite.h"

bool parseNetwork(vector<vector<string> > &data,   pair<Network, Network> &Networks,    BComposite &Genome, fileNetwork fileParam);

bool prepareMemory(Network*,BComposite*);


#endif // GPARSE_H
