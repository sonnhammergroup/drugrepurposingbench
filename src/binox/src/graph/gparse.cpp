#include "gparse.h"


#include <boost/algorithm/string.hpp>
#include <iostream>


bool parseNetwork(vector<vector<string> > &data,   pair<Network ,Network> &Networks,    BComposite &Genome, fileNetwork fileParam){



    map<BGene*, Network::Node> geneVertMap;
    LinkProperties link;
    NodeProperties vp;
    Network::Node v1, v2;

    string geneNameA, geneNameB;
    BGene* A;
    BGene* B;

    bool hasA, hasB;

    int fileSize = data.size();
    double score;

    for (int row = 0 ; row < fileSize; row++) {

        if(fileParam.header && row == 0)
            continue;

        geneNameA = data.at(row).at(fileParam.colGeneA);
        geneNameB = data.at(row).at(fileParam.colGeneB);
        score = atof(data.at(row).at(fileParam.colScore).c_str());
        if (!geneNameA.size() || !geneNameB.size() || ((double)score <= fileParam.cuttoff) || data.at(row).size() < fileParam.colSum)
            continue;


        to_upper(geneNameA);
        to_upper(geneNameB);

        BGene* geneA = new BGene(geneNameA);
        BGene* geneB = new BGene(geneNameB);

        if(!(BGene*)Genome.has(geneA))
            Genome.add(new BGene(geneNameA));

        if(!(BGene*)Genome.has(geneB))
            Genome.add(new BGene(geneNameB));


       A = (BGene*)Genome.get(geneA);
       B = (BGene*)Genome.get(geneB);

       hasA = A->inNetwork;
       hasB = B->inNetwork;

       if (!hasA && !hasB)
       {

           vp.gene = A;
           v1 = Networks.first.AddNode(vp);
           geneVertMap[vp.gene]= v1;

           vp.gene = B;
           v2 = Networks.first.AddNode(vp);
           geneVertMap[vp.gene]= v2;

           link.weight = score;
           Networks.first.AddLink(v1, v2, link, link);

           A->inNetwork=true;
           B->inNetwork=true;

      }
       else if(hasA && !hasB)
       {

           vp.gene = B;
           v2 = Networks.first.AddNode(vp);
           geneVertMap[vp.gene]= v2;

           v1 = geneVertMap[A];


           link.weight = score;
           Networks.first.AddLink(v1, v2, link, link);


           B->inNetwork=true;

       }
       else if (!hasA && hasB)
       {

           vp.gene = A;
           v1 = Networks.first.AddNode(vp);
           geneVertMap[vp.gene]= v1;

           v2 = geneVertMap[B];


           link.weight = score;
           Networks.first.AddLink(v1, v2, link, link);

           A->inNetwork=true;

       }
       else
       {

           v1 = geneVertMap[A];
           v2 = geneVertMap[B];


           link.weight = score;
           Networks.first.AddLink(v1, v2, link, link);

       }

       delete geneA;
       delete geneB;

    }

    Networks.second = Networks.first;

    Networks.first.id   = 0;
    Networks.second.id  = 1;

    return true;

}


bool prepareMemory(Network* origNetwork,BComposite* Genome)
{

    unsigned int size = Genome->size();
    unsigned int geneNr=0;

    for (BComposite::iterator it = Genome->begin(); it != Genome->end(); ++it) {
       ((BGene*)(*it))->setID(geneNr);
       ((BGene*)(*it))->initInteractors(size);
       geneNr++;
    }

    if(geneNr != size)
        return false;



    Network::Node v1, v2;
    for (Network::link_range_t er = origNetwork->getLinks(); er.first != er.second; er.first++)
    {
        origNetwork->getNodesByLink(*er.first, v1, v2);

        BGene* A = origNetwork->properties(v1).gene;
        BGene* B = origNetwork->properties(v2).gene;

        A->relGeneGene->setOrigLink(B->getId(),1);
        B->relGeneGene->setOrigLink(A->getId(),1);
    }


    return true;
}

