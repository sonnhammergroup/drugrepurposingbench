#ifndef GREAD_H
#define GREAD_H

#include "gbase.h"
#include "bin/bintypes.h"
#include "gtypes.h"

bool parseNetwork(vector<vector<string> > &data,   pair<Network, Network> &Networks,    BComposite &Genome, nwFile fileParam);

bool prepareMemory(Network*,BComposite*);


#endif // GREAD_H
