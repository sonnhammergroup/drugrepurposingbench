/*************************************************************
 *                   Christoph Ogris                         *
 *             christoph.ogris@scilifelab.se                 *
 *************************************************************/

#include "grandomize.h"
#include <time.h>       /* time */
#include <cstdio>

#define DEGREE_BIN(x)		((int)round(log(x)+1))

bool generateRandomNetworkFile(string method, pair <Network,Network> &networks , map<int, vector<Record> > &degRecordsMap){

     if(method=="SecondOrder"){
         if (!generateRandomNetworkSecondOrder(networks, degRecordsMap))
         {
             networks.second = networks.first;
             networks.second.id = 1;
             return false;
         }
     }
     else if(method=="LinkSwap"){
         if (!generateRandomNetworkLinkSwap(networks))
         {
             networks.second = networks.first;
             networks.second.id = 1;
             return false;
         }
     }
     else if(method=="NodeSwap"){
         if (!generateRandomNetworkLabelSwap(networks, degRecordsMap))
         {
             networks.second = networks.first;
             networks.second.id = 1;
             return false;
         }
    }
    else if(method=="Assignment"){
         if (!generateRandomNetworkAssignment(networks))
         {
             networks.second = networks.first;
             networks.second.id = 1;
             return false;
         }
     }
     else
         return false;

    return true;
}

bool generateRandomNetworkLinkSwap(pair <Network,Network> &networks){

        vector< Network::Link> containerLinks;
        LinkProperties link;
        link.weight = 1.0;

        for ( Network::link_range_t er = networks.second.getLinks(); er.first != er.second; er.first++)
            containerLinks.push_back(*er.first);

        int randIndex1, randIndex2;
        bool test;

        Network::Node v1, v2, v3, v4;
        map<string, bool> hasTested;
        stringstream ss;
        int countSwaps = 0;

        while(hasTested.size()/2 <= containerLinks.size() && containerLinks.size() >= 2)
        {
            //hasTested.clear();
            do
            {
                randIndex1 = rand()%containerLinks.size();
                randIndex2 = rand()%containerLinks.size();

                networks.second.getNodesByLink(containerLinks.at(randIndex1), v1, v2);
                networks.second.getNodesByLink(containerLinks.at(randIndex2), v3, v4);

                test = (randIndex1 != randIndex2)
                    && ((!networks.second.hasLink(v1, v3) && !networks.second.hasLink(v2, v4))
                    || (!networks.second.hasLink(v1, v4) && !networks.second.hasLink(v2, v3)))
                    && v1 != v3 && v2 != v3 && v4 != v1 && v4 != v2;

                if (!test && (randIndex1 != randIndex2))
                {
                    ss << randIndex1 << " " << randIndex2;
                    hasTested[ss.str()] = true;
                    ss << randIndex2 << " " << randIndex1;
                    hasTested[ss.str()] = true;
                }


            } while (!test && hasTested.size()/2 <= containerLinks.size() && containerLinks.size()>=2);

            if (test)
            {
                if (!networks.second.hasLink(v1, v3) && !networks.second.hasLink(v2, v4))
                {
                    networks.second.RemoveLink(v1, v2);
                    networks.second.RemoveLink(v3, v4);
                    networks.second.AddLink(v1, v3, link);
                    networks.second.AddLink(v2, v4, link);
                    countSwaps++;
                }
                else if (!networks.second.hasLink(v1, v4) && !networks.second.hasLink(v2, v3))
                {
                    networks.second.RemoveLink(v1, v2);
                    networks.second.RemoveLink(v3, v4);
                    networks.second.AddLink(v1, v4, link);
                    networks.second.AddLink(v2, v3, link);
                    countSwaps++;
                }
                if (randIndex1 > randIndex2)
                {
                    containerLinks.erase(containerLinks.begin()+randIndex1);
                    containerLinks.erase(containerLinks.begin()+randIndex2);
                    hasTested.clear();
                }
                else
                {
                    containerLinks.erase(containerLinks.begin()+randIndex2);
                    containerLinks.erase(containerLinks.begin()+randIndex1);
                    hasTested.clear();
                }
            }
        }

    return true;
}

bool generateRandomNetworkLabelSwap(pair <Network,Network> &networks , map<int, vector<Record> > &degRecordsMap){

    vector<Record> randRecords;

     //create a list of vertices paired with the connectivity
     //as in the original network (refered to as records)
     for (Network::node_range_t vr = networks.second.getNodes(); vr.first != vr.second; vr.first++)
     {
         Record record;
         record.node = *vr.first;

         record.degree = networks.second.getNodeDegree(record.node); //at this point networks.second is exact copy of networks.first
         randRecords.push_back(record);
     }

     //randomize the list of records
     random_shuffle(randRecords.begin(), randRecords.end());

     int randIndex;
     BGene* swaplabel;
     vector<Record> *vec1;
     for (int k = 0; k < (int)randRecords.size(); k++)
     {
         vec1 = &(degRecordsMap[DEGREE_BIN(randRecords.at(k).degree)]);
         randIndex = rand()%(vec1->size());

         swaplabel = networks.second.properties(randRecords.at(k).node).gene;

         networks.second.properties(randRecords.at(k).node).gene = networks.second.properties((*vec1).at(randIndex).node).gene;
         networks.second.properties((*vec1).at(randIndex).node).gene = swaplabel;
     }

     return true;
}


bool generateRandomNetworkAssignment(pair <Network,Network> &networks ){

       int randNum, randIndex;
       vector<Record> randRecordsAll ;
       vector<int> randRecordIndices;
       LinkProperties link;
       bool test = true, doBreak = false;

       //cout << "Generating random network... "<<endl;

       link.weight = 1.0;

       //create a list of vertices paired with the connectivity
       //as in the original network (refered to as records)
       for (Network::node_range_t vr = networks.second.getNodes(); vr.first != vr.second; vr.first++)
       {
           Record record;
           record.node = *vr.first;
           record.degree = networks.second.getNodeDegree(record.node); //at this point networks.second is exact copy of networks.first
           randRecordsAll.push_back(record);
       }

       //randomize the list of records
       random_shuffle(randRecordsAll.begin(), randRecordsAll.end());

       //clear the random network of all links
       networks.second.RemoveAllLinks();

       for (int k = 0; k < (int)randRecordsAll.size();k++)
       {
           for (int j = 0; j < (int)networks.second.properties(randRecordsAll.at(k).node).connectedDegrees.size(); j++)
           {
               //generate a list of indices of randRecords
               randRecordIndices.clear();
               for (int i = 0; i < (int)randRecordsAll.size(); i++)
                   randRecordIndices.push_back(i);

               //pick a random record index out of the list of indices
               randIndex = rand()%randRecordIndices.size();
               randNum = randRecordIndices.at(randIndex);
               doBreak = false;

               //test if this  is ok to connect to (!test is ok)
               test = (k == randNum
                       || networks.second.hasLink(randRecordsAll.at(k).node, randRecordsAll.at(randNum).node)
                       || networks.second.getNodeDegree(randRecordsAll.at(k).node) == randRecordsAll.at(k).degree
                       || networks.second.getNodeDegree(randRecordsAll.at(randNum).node) == randRecordsAll.at(randNum).degree);


               while(test)
               {
                   randRecordIndices.erase(randRecordIndices.begin()+randIndex);

                   if (!randRecordIndices.size())
                       break;

                   randIndex = rand()%randRecordIndices.size();
                   randNum = randRecordIndices.at(randIndex);

                   test = (k == randNum
                           || networks.second.hasLink(randRecordsAll.at(k).node, randRecordsAll.at(randNum).node)
                           || networks.second.getNodeDegree(randRecordsAll.at(k).node) == randRecordsAll.at(k).degree
                           || networks.second.getNodeDegree(randRecordsAll.at(randNum).node) == randRecordsAll.at(randNum).degree);
               }

               if (!test)
               {
                   //add the link to the network
                   networks.second.AddLink(randRecordsAll.at(k).node, randRecordsAll.at(randNum).node, link);

                   //instead of checking all the records, only check two (the extra case is if k < randNum and k is removed)
                   if (randRecordsAll.at(k).degree == networks.second.getNodeDegree(randRecordsAll.at(k).node))
                   {
                       randRecordsAll.erase(randRecordsAll.begin()+k);
                       doBreak = true;
                   }
                   else if (randRecordsAll.at(randNum).degree == networks.second.getNodeDegree(randRecordsAll.at(randNum).node))
                   {
                       randRecordsAll.erase(randRecordsAll.begin()+randNum);
                       doBreak = true;
                   }
                   else if (randNum > 0)
                       if (randRecordsAll.at(randNum).degree == networks.second.getNodeDegree(randRecordsAll.at(randNum).node))
                       {
                           randRecordsAll.erase(randRecordsAll.begin()+(randNum));
                           doBreak = true;
                       }

                   if (doBreak)
                   {
                       if (randRecordIndices.size())
                           k --;
                       break;
                   }
               }

               if (!randRecordsAll.size() || !randRecordIndices.size())
                   break;

           }//end for j
       }//end for k

       //validate and fix the connectivity errors
       vector<pair<Network::Node, Network::Node> > errors;
       if (!validateConnectivities(networks, errors))
           fixConnectivityErrors(networks, errors);

       if (validateConnectivities(networks, errors))
           return true;


       int sum = 0;
       for (int i = 0; i < (int)errors.size(); i++)
           sum += (int)abs(networks.first.getNodeDegree(errors.at(i).first) - networks.second.getNodeDegree(errors.at(i).second));
      // printf("***Warning*** Randomization failed to conserve connectivities.\n");
      // printf("***Warning*** There was a difference of %d links between the original and randomized network\n", sum);

       return false;

}

bool generateRandomNetworkSecondOrder(pair <Network,Network> &networks , map<int, vector<Record> > &degRecordsMap)
{
    int randNum, randIndex;
    vector<Record> randRecordsAll, randRecordsAvail;
    vector<int> randRecordIndices;
    LinkProperties link;
    bool test = true;
    link.weight = 1.0;

    /* NOTE: Assuming that at this point networks.second = networks.first!!!!! */

    //create a list of vertices paired with the connectivity
    //as in the original network (refered to as records)
    for (Network::node_range_t vr = networks.second.getNodes(); vr.first != vr.second; vr.first++)
    {
        Record record;
        record.node = *vr.first;
        record.degree = networks.second.getNodeDegree(record.node); //at this point networks.second is exact copy of networks.first
        randRecordsAll.push_back(record);
    }


    //randomize the list of recordsd::find(v.begin(), v.end(), x) !=
    random_shuffle(randRecordsAll.begin(), randRecordsAll.end());

    //clear the random network of all links
    networks.second.RemoveAllLinks();

    for (int k = 0; k < (int)randRecordsAll.size(); k++)
    {
        for (int j = 0; j < (int)networks.second.properties(randRecordsAll.at(k).node).connectedDegrees.size(); j++)
        {
            //cout << (int)networks.second.properties(randRecordsAll.at(k).node).connectedDegrees.size() << "\t"<< networks.second.properties(randRecordsAll.at(k).node).gene->countOrigDegree()<<endl ;

            //cout << networks.second.properties(randRecordsAll.at(k).node).connectedDegrees.at(j);
            //cout << networks.second.properties(randRecordsAll.at(k).node).gene->;
            //exit(1);

            randRecordsAvail = degRecordsMap[networks.second.properties(randRecordsAll.at(k).node).connectedDegrees.at(j)];

            //generate a list of indices of randRecordsAvail indices
            randRecordIndices.clear();
            for (int i = 0; i < (int)randRecordsAvail.size(); i++)
                randRecordIndices.push_back(i);

            //pick a random record index out of the list of indices
            randIndex = rand()%randRecordIndices.size();
            randNum = randRecordIndices.at(randIndex);

            //test if this  is ok to connect to (!test is ok)
            test = (randRecordsAll.at(k).node == randRecordsAvail.at(randNum).node
                    || networks.second.hasLink(randRecordsAll.at(k).node, randRecordsAvail.at(randNum).node)
                    || networks.second.getNodeDegree(randRecordsAll.at(k).node) == randRecordsAll.at(k).degree
                    || networks.second.getNodeDegree(randRecordsAvail.at(randNum).node) == randRecordsAvail.at(randNum).degree);

            //if not find a different one and delete that index out of the list of record indices
            while(test)
            {
                randRecordIndices.erase(randRecordIndices.begin()+randIndex);

                if (!randRecordIndices.size())
                    break;

                randIndex = rand()%randRecordIndices.size();
                randNum = randRecordIndices.at(randIndex);

                test = (randRecordsAll.at(k).node== randRecordsAvail.at(randNum).node
                        || networks.second.hasLink(randRecordsAll.at(k).node, randRecordsAvail.at(randNum).node)
                        || networks.second.getNodeDegree(randRecordsAll.at(k).node) == randRecordsAll.at(k).degree
                        || networks.second.getNodeDegree(randRecordsAvail.at(randNum).node) == randRecordsAvail.at(randNum).degree);
            }

            if (!test)
            {
                bool doBreak = false;

                //add the link to the network
                networks.second.AddLink(randRecordsAll.at(k).node, randRecordsAvail.at(randNum).node, link);

                //instead of checking all the records, only check two (the extra case is if k < randNum and k is removed)
                if (randRecordsAll.at(k).degree == networks.second.getNodeDegree(randRecordsAll.at(k).node))
                {
                    randRecordsAll.erase(randRecordsAll.begin()+k);
                    doBreak = true;
                }
                if (randRecordsAvail.at(randNum).degree == networks.second.getNodeDegree(randRecordsAvail.at(randNum).node))
                {
                    randRecordsAvail.erase(randRecordsAvail.begin()+randNum);
                    doBreak = true;
                }
                if (doBreak)
                {
                    if (randRecordIndices.size())
                        k --;
                    break;
                }
            }

            if (!randRecordsAll.size() || !randRecordsAvail.size() || !randRecordIndices.size())
                break;

        }//end for j
    }//end for k

    //validate and fix the connectivity errors
    vector<pair<Network::Node, Network::Node> > errors;

    if (!validateConnectivities(networks, errors))
        fixConnectivityErrors(networks, errors);

    if (validateConnectivities(networks, errors))
       return true;

    int sum = 0;
    for (int i = 0; i < (int)errors.size(); i++)
        sum += (int)abs(networks.first.getNodeDegree(errors.at(i).first) - networks.second.getNodeDegree(errors.at(i).second));
   // printf("***Warning*** Randomization failed to conserve connectivities.\n");
   // printf("***Warning*** There was a difference of %d links between the original and randomized network\n", sum);

    return false;
}



void fixConnectivityErrors(pair<Network, Network> &networks, vector<pair<Network::Node, Network::Node> > &errors)
{
    int numToGo;
    Network::link_range_t er;
    LinkProperties link;
    bool test = false;

    Network::Node v1=NAN ,v2=NAN;

    link.weight = 1.0;


    //This algorithm has two parts.
    //First, look for nodes that have an odd number of connectivity errors. (There should be an even number of these)
    //In order to fix these errors, you have to break a link to a node with only one
    //connected link. Then you can form an link between the node in errors and the one with odd degree
    //and use the other node to connect to the next node in errors that has odd connectivity error.
    //Second, after the odd errors are fixed, it fixes the even connectivity errors by finding sufficient links
    //to break and adding both links to the node with even errors until it has the correct connectivity.

    //this case fixes all of the odd numbered links first
    for (int i = 0; i < (int)errors.size(); i++)
    {
        numToGo = networks.first.getNodeDegree(errors.at(i).first) - networks.second.getNodeDegree(errors.at(i).second);
        if (numToGo % 2 == 1)
        {
            int nextOdd = -1;
            for (int k = i+1; k < (int)errors.size(); k++)
                if ((networks.first.getNodeDegree(errors.at(k).first) - networks.second.getNodeDegree(errors.at(k).second)) % 2 == 1)
                    nextOdd = k;

            if (nextOdd == -1) //should not happen, just for safety.
                break;

            er = networks.second.getLinks();
            for (; er.first != er.second; )
            {
                networks.second.getNodesByLink(*er.first, v1, v2);

                test = (networks.second.getNodeDegree(v1)%2 == 1) || (networks.second.getNodeDegree(v2)%2 == 1);
                test = test && ((v1 != errors.at(i).second && v2 != errors[nextOdd].second) && (v1 != errors[nextOdd].second && v2 != errors.at(i).second));
                test = test && (((!networks.second.hasLink(v1, errors.at(i).second) && !networks.second.hasLink(v2, errors[nextOdd].second)) || (!networks.second.hasLink(v1, errors[nextOdd].second) && !networks.second.hasLink(v2, errors.at(i).second))));

                er.first++;
                if (test)
                    break;
            }

            // in this case there will always be an even number of errors with
            // odd numbers of connections
            if (test)
            {
                bool didAdd = false;
                if (!networks.second.hasLink(v1, errors.at(i).second) && !networks.second.hasLink(v2, errors[nextOdd].second))
                {
                    networks.second.AddLink(v1, errors.at(i).second, link, link);
                    networks.second.AddLink(v2, errors[nextOdd].second, link, link);
                    didAdd = true;
                }
                else if (!networks.second.hasLink(v2, errors.at(i).second) && !networks.second.hasLink(v1, errors[nextOdd].second))
                {
                    networks.second.AddLink(v2, errors.at(i).second, link, link);
                    networks.second.AddLink(v1, errors[nextOdd].second, link, link);
                    didAdd = true;
                }
                if (didAdd)
                    networks.second.RemoveLink(v1, v2);
            }//end if test
        }//end if odd
    }

    //this case fixes all of the even ones.
    for (int i = 0; i < (int)errors.size(); i++)
    {
        numToGo = networks.first.getNodeDegree(errors.at(i).first) - networks.second.getNodeDegree(errors.at(i).second);
        //at this point numToGo should be even
        for (int j = 0; j < (numToGo/2); j++)
        {
            if (j == 0) er = networks.second.getLinks();
            for (; er.first != er.second;)
            {
                networks.second.getNodesByLink((*er.first), v1, v2);
                test = (v1 != errors.at(i).second
                     && v2 != errors.at(i).second
                     && !networks.second.hasLink(v1, errors.at(i).second)
                     && !networks.second.hasLink(v2, errors.at(i).second));

                er.first++;
                if (test)
                    break;
            }

            if (test)
            {
                networks.second.RemoveLink(v1, v2);
                networks.second.AddLink(v1, errors.at(i).second, link, link);
                networks.second.AddLink(v2, errors.at(i).second, link, link);
            }
        }
    }

}

bool validateConnectivities(pair<Network, Network> &networks, vector<pair<Network::Node, Network::Node> > &errors)
{
    bool valid = true;
    Network::Node v1, v2;

    errors.clear();
    for (Network::node_range_t vp1 = networks.first.getNodes(), vp2 = networks.second.getNodes(); vp1.first != vp1.second && vp2.first != vp2.second; )
    {

        v1 = *vp1.first;
        v2 = *vp2.first;
        if (networks.first.properties(v1).gene != networks.second.properties(v2).gene)
        {
            valid = false;
            printf("This should never happen...\n");
            exit(1);
        }


        if (networks.first.getNodeDegree(v1) != networks.second.getNodeDegree(v2))
        {
            int i = 0;
            pair<Network::Node, Network::Node> thisPair;
            thisPair.first = v1;
            thisPair.second = v2;
            //sort the errors by increasing difference in connectivity from original network
            if (errors.size())
                for (i = 0; i < (int)errors.size(); i++)
                    if (networks.first.getNodeDegree(errors.at(i).first) - networks.second.getNodeDegree(errors.at(i).second) >
                        networks.first.getNodeDegree(v1) - networks.second.getNodeDegree(v2))
                            break;

            errors.insert(errors.begin()+i, thisPair);
            valid = false;
        }
        vp1.first++;
        vp2.first++;
    }


    return (valid && (networks.first.getNodeCount() == networks.second.getNodeCount()) && (networks.first.getLinkCount() == networks.second.getLinkCount()));
}
