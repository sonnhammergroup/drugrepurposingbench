/*************************************************************
 *                   Christoph Ogris                         *
 *             christoph.ogris@scilifelab.se                 *
 *************************************************************/

#ifndef __GTYPES_H__
#define __GTYPES_H__

#include "gboost.h"
#include <vector>

class BGene;

class NodeProperties
{
    public:
        NodeProperties(){}
        ~NodeProperties(){}

        BGene* gene;
        std::vector<int> connectedDegrees;

};

class LinkProperties
{
    public:
        LinkProperties(){}
        ~LinkProperties(){}

        float weight;
        double pfc;
};

typedef BoostGraph< NodeProperties, LinkProperties> Network;

struct Record
{
    Network::Node node;
    int degree;
};


#endif
