set(utils_source_files
ubase.h
utypes.h
uerror.cpp
uargparser.cpp
uio.cpp
ucalc.cpp
uprint.cpp
)
add_library(utils ${utils_source_files})
