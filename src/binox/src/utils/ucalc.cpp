/*************************************************************
 *                   Christoph Ogris                         *
 *             christoph.ogris@scilifelab.se                 *
 *************************************************************/

#include "ucalc.h"

#include <boost/math/distributions/binomial.hpp>

using boost::math::binomial;

//calculating significance of how likely it is that the number of links follows a random model

/* computes Binomial CDF:
* n represents the min degree of outgoing links of group A OR group B
* k is set to the amount of connections between group A AND group B
* mu is the number of expected links which get approximated through the null hypothesis.
*
* IFF it is more likley that the overlapp occures more that random the uppertail gets calculated
*/
bool binomialCDF(int n, int k, long double mu, long double &uppertail)
{

    if(n <= 0)
        return false;

    if(k < 0)
        return false;

    if(mu <= k) // Enrichment
    {
        long double p = mu / ((long double) n);

        binomial dist(n,p);
        uppertail = cdf(complement(dist, k-1));

        return true;
    }

    if(mu > k) // Depletion
    {
        long double p = mu / ((long double) n);

        binomial dist(n,p);
        uppertail = cdf(dist, k);

        return true;
    }

    return false;
}

bool binomialCDF_single(int n, int k, long double mu, long double &tail, bool enrich)
{

    if(n <= 0 || k < 0)
        return false;

    long double p = mu / ((long double) n);

    if (enrich) {
        tail = cdf(complement(binomial(n, p), k-1));
    } else {
        tail = cdf(binomial(n, p), k);
    }

    return true;
}

bool sortVectorOfPtrDecreasing(long double* a,long double* b)
{
    return *a > *b;
}




bool pvalueAdjustBH(vector<long double *> &pval){

    if(pval.size()==1)
        return true;


    int size = pval.size();

    //getting rid of NaN
    vector<long double *> noNaN;
    for (int j = 0; j < size; j++)
        {
            if(*pval.at(j)==*pval.at(j))
                noNaN.push_back(pval.at(j));
        }


    int sizeNoNaN = noNaN.size();

    std::sort(noNaN.begin(), noNaN.end(), sortVectorOfPtrDecreasing);


    for (int i = 0; i < sizeNoNaN; i++)
        {
            //FDR CALC:
            //*noNaN.at(i) *= (   (long double)size) /   ((long double)sizeNoNaN - (long double)i) ;
            //FWER
            *noNaN.at(i) *= (long double)size;
            if(*noNaN.at(i) > 1)
                  *noNaN.at(i) = 1;
        }

    return true;
}





