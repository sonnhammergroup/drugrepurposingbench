/*************************************************************
 *                   Christoph Ogris                         *
 *             christoph.ogris@scilifelab.se                 *
 *************************************************************/

#ifndef UIO_H
#define UIO_H


#include "ubase.h"

#include <sstream>

bool readFile(string path, string sep, vector<vector<string> > & data);

bool writeFile( vector<vector<string> > data, fileInfo out);

bool createDir(string path, bool replace);

bool checkDir(string path);


bool getAbsolutePath(string &path);
bool getFileName(string &path);


template <class T>
inline std::string to_string (const T& t)
{
std::stringstream ss;
ss << t;
return ss.str();
}

#endif // UIO_H
