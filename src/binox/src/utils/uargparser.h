/*************************************************************
 *                   Christoph Ogris                         *
 *             christoph.ogris@scilifelab.se                 *
 *************************************************************/

#ifndef UARGPARSER_H
#define UARGPARSER_H

#include "utypes.h"

bool uInitArgParser(int argc, char **argv, BinoXConfig &config);

#endif // UARGPARSER_H
