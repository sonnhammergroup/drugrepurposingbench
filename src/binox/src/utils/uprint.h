/*************************************************************
 *                   Christoph Ogris                         *
 *             christoph.ogris@scilifelab.se                 *
 *************************************************************/

#ifndef UPRINT_H
#define UPRINT_H

#include "utypes.h"


using namespace std;

void printBanner();

void printTime(string action, float time);

void printErrorAndExit(string reason);

#endif // UPRINT_H
