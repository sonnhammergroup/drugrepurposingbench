/*************************************************************
 *                   Christoph Ogris                         *
 *             christoph.ogris@scilifelab.se                 *
 *************************************************************/

#ifndef UCALC_H
#define UCALC_H
#include "ubase.h"


#define DEGREE_BIN(x)		((int)round(log(x)+1))
#define MIN(a,b) ((a<b)?a:b)
#define MAX(a,b) ((a>b)?a:b)

bool binomialCDF(int n, int k, long double mu, long double &uppertail);

bool binomialCDF_single(int n, int k, long double mu, long double &tail, bool enrich);

bool pvalueAdjustBH(vector<long double*> &pval);

bool sortVectorOfPtrDecreasing(long double *a, long double *b);



#endif // UCALC_H
