/*************************************************************
 *                   Christoph Ogris                         *
 *             christoph.ogris@scilifelab.se                 *
 *************************************************************/

#include "uprint.h"
#include <iostream>
#include <iomanip>
#include <sstream>
#include <cstdlib>

void printBanner()
{
    cout << endl;
    cout << "oooooooooo.   o8o                        ooooooo  ooooo"<< endl ;
    cout << "`888'   `Y8b  `\"'                         `8888    d8'"<< endl ;
    cout << " 888     888 oooo  ooo. .oo.    .ooooo.     Y888..8P"<< endl ;
    cout << " 888oooo888' `888  `888P\"Y88b  d88' `88b     `8888'"<< endl ;
    cout << " 888    `88b  888   888   888  888   888    .8PY888."<< endl ;
    cout << " 888    .88P  888   888   888  888   888   d8'  `888b"<< endl ;
    cout << " o888bood8P'  o888o o888o o888o `Y8bod8P' o888o  o88888o"<< endl ;
        cout << endl;

    }

void printTime(string action,float time)
{

    if(time/(float)60 < 1)
        cout << setw(20)<< time                      << setw(5) << " sec";
    else if(time/(float)3600 < 60)
        cout << setw(20)<< time/(float)60             << setw(5) << " min" ;
    else if(time/(float)216000 < 1)
        cout << setw(20)<< time/(float)216000          << setw(5)<< " h";
    else if(time/(float)(216000*24) < 1)
        cout << setw(20)<< time/(float)(216000*24)      << setw(5) << " days" ;

    cout << setw(25) << action << "\n";
}

void printErrorAndExit(string reason)
{
    cout << endl;
    cout << "ERROR" << endl;
    cout << setw(10) << reason << endl;
    exit(1);
}
