/*************************************************************
 *                   Christoph Ogris                         *
 *             christoph.ogris@scilifelab.se                 *
 *************************************************************/

#ifndef UTYPES_H
#define UTYPES_H

#include <string>
#include <vector>

struct fileInfo
{
    //File path
    std::string absolutePath;
    std::string fileName;

    //File information
    bool header;
    std::string sep;
    std::string skipSign;
};

struct networkFile
{
    //File structure
    fileInfo io;

    //Column def
    unsigned int colGeneA;
    unsigned int colGeneB;  
    unsigned int colLinkWeight;
    unsigned int colSum;

};

struct stFile
{
    //def cutoff
    double cutOff;
};

struct groupFile
{
    //File structure
    fileInfo io;

    // In case of an st file col Sum and col Group will be set respectivly
    bool isStFile;
    stFile stProp;

    //Column def
    unsigned int colSum;
    unsigned int colGene;
    unsigned int colGroup;

};



struct resultFile
{
    //File structure
    fileInfo io;

    //format
    std::string fileFormat;
};

struct processSetting
{
    //Generall
    std::string date;

    //Network information
    unsigned int linkCount;
    unsigned int nodeCount;
    double cutOff;

    //Randomization
    std::string method;
    unsigned int iterations;
    unsigned int seed;

    //Statistical Analysis
    std::string relationType;
    unsigned int minSetSize;

    //avoids double calculation. only A-B not B-A
    bool intra;

    unsigned int failedRandomizations;
};

struct BinoXConfig
{
    //Version
    std::string version;

    //Network information
    networkFile fileFormatNetwork;

    //Groupfiles
    groupFile fileFormatGroupA;
    groupFile fileFormatGroupB;

    //Process Settings
    processSetting process;

    //Result file output
    resultFile fileFormatResult;

    //Pre processed Network
    fileInfo filePreProcessedNetwork;

    bool newNetwork;
    bool preProcessedNetwork;
    bool createPreProcessedNetwork;
    bool evaluateGroups;
    bool stAnalysisMode;

    bool DEBUG;
};



#endif // UTYPES_H
