# Title     : anubix_tidy.R
# Objective : re-implement a vectorized version of ANUBIX (Miguel C et al)
# Created by: Dimitri
# Created on: 2020-04-12
library(tidyverse)
library(optimr)
library(stats)
library(TailRank)


crosstalk <- function(links_triple, drug_target_sets, disease_gene_sets){
  geneset_genes_diseases <- distinct(disease_gene_sets, disease) %>%
    mutate(dummy = TRUE) %>%
    full_join(distinct(drug_target_sets, node) %>% mutate(dummy = TRUE), by = "dummy") %>%
    dplyr::select(node, disease)

  # Calculate the crosstalk between a geneset and a pathway
  geneset_genes_diseases %>%
    left_join(links_triple, by = c("node", "disease")) %>%
    right_join(drug_target_sets, by = "node") %>%
    group_by(drug, disease) %>%
    summarize(crosstalk = sum(crosstalk, na.rm = TRUE))
}

sample_nodes <- function(node_name, bin_num, num_samples) {
  bins %>%
    filter(bin == bin_num) %>%
    filter(node != node_name) %>%
    sample_n(size = num_samples, replace = TRUE) %>%
    dplyr::select(node) %>%
    rename(sample_node = node)
}

log_likelihood <- function(inits, x) {
  A <- inits[1]
  B <- inits[2]
  Y <- x[, 3]
  N <- x[, 2]
  -sum(lgamma(abs(A) + abs(B)) - lgamma(abs(A)) - lgamma(abs(B)) +
         lgamma(Y + abs(A)) + lgamma(N - Y + abs(B)) - lgamma(N + abs(A) + abs(B)), na.rm = TRUE)
}


fit_betabinomial <- function(random_values, observed_crosstalk, max_crosstalk) {
  n_samples <- length(random_values)
  n <- max_crosstalk
  m_1 <- mean(random_values, na.rm = TRUE)
  m_2 <- mean(random_values^2, na.rm = TRUE)

  alpha <- (n * m_1 - m_2) / (n * (m_2 / m_1 - m_1 - 1) + m_1)
  beta <- (n - m_1) * (n - m_2/m_1) / (n * (m_2 / m_1 - m_1 - 1) + m_1)
  initial_parameters <- abs(c(alpha, beta))

  # Make sure the starting values are ok
  initial_parameters[initial_parameters == 0 | is.na(initial_parameters) | is.infinite(initial_parameters)] <- 10

  dat <- as.data.frame(cbind(1:n_samples, rep(max_crosstalk, n_samples), random_values))
  optimized <- optimr(initial_parameters, fn = log_likelihood, x = dat)
  if (any(is.na(optimized$par))) {
    print(optimized$par)
    print(initial_parameters)
    print(random_values)
    optimized <- optimr(c(10, 10), fn = log_likelihood, x = dat)
  }
  optimized$par <- abs(optimized$par)
  betabinomial_out(observed_crosstalk, max_crosstalk, optimized$par[1], optimized$par[2])
}


betabinomial_out <- function(observed_crosstalk, max_crosstalk, alpha, beta) {
  bb_mean <- max_crosstalk * alpha / (alpha + beta)
  bb_variance <- bb_mean * beta * (alpha + beta + max_crosstalk) / ((alpha + beta) * (alpha + beta + 1))
  bb_z <- (observed_crosstalk - bb_mean) / sqrt(bb_variance)
  bb_p <- 0.5 * dbb(observed_crosstalk, max_crosstalk, alpha, beta) +
    sum(dbb((observed_crosstalk + 1) : max_crosstalk, max_crosstalk, alpha, beta), na.rm = TRUE)
  output <- c(bb_mean, bb_variance, bb_z, bb_p)
  names(output) <- c("mean_bb", "var_bb", "z_bb", "p_bb")
  output
}


anubix_tidy <- function(links_triple, drugs, diseases, out_file = NA, combinations = NA, n_samples = 10, chunks = 1) {
  # Calculate the max and observed crosstalk for every drug-disease pair
  if (!is_tibble(combinations)) {
    combinations <- count(diseases, disease) %>%
    mutate(dummy = TRUE) %>%
    full_join(count(drugs, drug) %>% mutate(dummy = TRUE), by = "dummy")
  }

  drugs <- filter(drugs, drug %in% combinations$drug)
  diseases <- filter(diseases, disease %in% combinations$disease)
  links_triple <- filter(links_triple, disease %in% combinations$disease)

  print("Calculating observed crosstalk.")
  # merge with drugs and diseases to get the correct number of genes in each and then remove all after calculation
  crosstalk_df <- combinations %>%
    mutate(max_crosstalk = n.x * n.y + max(n.x, n.y, na.rm = TRUE)) %>%
    dplyr::select(disease, drug, max_crosstalk) %>%
    left_join(crosstalk(links_triple, drugs, diseases), by = c("drug", "disease"))

  print("Preparing random samples.")
  final_crosstalk <- drugs %>%
    left_join(bins) %>%
    group_by(node, drug, bin) %>%
    summarise(node_sample = list(sample_nodes(node, bin, num_samples = n_samples))) %>%
    unnest(cols = node_sample) %>%
    add_column(sample_number = rep(1:n_samples, times = nrow(drugs))) %>%
    ungroup() %>%
    dplyr::select(sample_node, drug, sample_number) %>%
    dplyr::rename(node = sample_node) %>%
    group_by(sample_number) %>%
    group_modify(~crosstalk(links_triple, ., diseases)) %>%
    ungroup() %>%
    dplyr::rename(sample_crosstalk = crosstalk) %>%
    right_join(crosstalk_df, by = c("drug", "disease"))

  print("Calculating statistics.")
  if (chunks == 1) {
    final_crosstalk %>%
      group_by(drug, disease) %>%
      summarise(
        observed_x = mean(crosstalk, na.rm = TRUE),
        max_x = mean(max_crosstalk, na.rm = TRUE),
        fit = list(fit_betabinomial(random_values = sample_crosstalk, observed_crosstalk = observed_x,
                                    max_crosstalk = max_x))
      ) %>%
      ungroup() %>%
      unnest_wider(col = fit) %>%
      mutate(fdr_bb = p.adjust(p_bb, method = "BH")) %>%
      write_tsv(out_file)
  } else {
    write(str_c("drug", "disease", "observed_x", "max_x", "mean_bb", "var_bb", "z_bb", "p_bb", "fdr_bb", sep = "\t"),
          file = out_file, sep = "\n")

    final_crosstalk_list <- final_crosstalk %>%
      distinct(drug) %>%
      split(1:chunks)

    for (element in seq_along(final_crosstalk_list)) {
      print(str_interp("Calculating elment ${element} out of ${chunks}"))
      final_crosstalk %>%
        filter(drug %in% final_crosstalk_list[[element]]$drug) %>%
        group_by(drug, disease) %>%
        summarise(
          observed_x = mean(crosstalk, na.rm = TRUE),
          max_x = mean(max_crosstalk, na.rm = TRUE),
          fit = list(fit_betabinomial(random_values = sample_crosstalk, observed_crosstalk = observed_x,
                                      max_crosstalk = max_x))
        ) %>%
        ungroup() %>%
        unnest_wider(col = fit) %>%
        mutate(fdr_bb = p.adjust(p_bb, method = "BH")) %>%
        write_tsv(out_file, append = TRUE, col_names = FALSE)
    }
  }
}

